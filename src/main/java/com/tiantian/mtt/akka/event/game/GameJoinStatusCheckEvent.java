package com.tiantian.mtt.akka.event.game;

import com.tiantian.mtt.akka.event.GameEvent;

/**
 *
 */
public class GameJoinStatusCheckEvent implements GameEvent {
    private String gameId;
    private String userId;

    public GameJoinStatusCheckEvent() {
    }

    public GameJoinStatusCheckEvent(String gameId, String userId) {
        this.gameId = gameId;
        this.userId = userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
