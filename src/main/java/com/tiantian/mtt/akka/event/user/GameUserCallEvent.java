package com.tiantian.mtt.akka.event.user;

import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserCallEvent implements GameUserEvent {

    private String userId;
    private String pwd;
    private String gameId;
    private String tableId;
    public GameUserCallEvent() {
    }

    public GameUserCallEvent(String userId, String pwd, String gameId) {
        this.userId = userId;
        this.pwd = pwd;
        this.gameId = gameId;
    }


    @Override
    public String event() {
        return "userCall";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
