package com.tiantian.mtt.akka.event.user;

import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserFastRaiseEvent implements GameUserEvent {
    private String userId;
    private String type;
    private String pwd;
    private String gameId;
    private String tableId;

    public GameUserFastRaiseEvent() {
    }

    public GameUserFastRaiseEvent(String userId, String type, String pwd, String gameId) {
        this.userId = userId;
        this.type = type;
        this.pwd = pwd;
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String event() {
        return "userFastRaise";
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
