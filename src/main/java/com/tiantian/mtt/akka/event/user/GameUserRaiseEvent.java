package com.tiantian.mtt.akka.event.user;

import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserRaiseEvent implements GameUserEvent {
    private String userId;
    private long raise;
    private String pwd;
    private String gameId;
    private String tableId;

    public GameUserRaiseEvent() {
    }

    public GameUserRaiseEvent(String userId, long raise, String pwd, String gameId) {
        this.raise = raise;
        this.pwd = pwd;
        this.userId = userId;
        this.gameId = gameId;
    }


    @Override
    public String event() {
        return "userRaise";
    }

    public long getRaise() {
        return raise;
    }

    public String getPwd() {
        return pwd;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setRaise(long raise) {
        this.raise = raise;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
