package com.tiantian.mtt.akka.event.user;


import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserEnterEvent implements GameUserEvent {
    private String gameId;
    private String userId;
    private String tableId;
    public GameUserEnterEvent() {
    }
    public GameUserEnterEvent(String gameId, String userId) {
        this.gameId = gameId;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    @Override
    public String event() {
        return "gameUserEnter";
    }

    @Override
    public String userId() {
        return userId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
