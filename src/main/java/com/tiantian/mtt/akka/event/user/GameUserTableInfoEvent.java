package com.tiantian.mtt.akka.event.user;

import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 * 玩家获取自己当前桌子信息
 */
public class GameUserTableInfoEvent implements GameUserEvent {
    private String gameId;
    private String userId;
    private String tableId;

    public GameUserTableInfoEvent() {
    }

    public GameUserTableInfoEvent(String gameId, String userId) {
        this.gameId = gameId;
        this.userId = userId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "tableInfo";
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
