package com.tiantian.mtt.akka.event.user;

import com.tiantian.mtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserDelaySignEvent implements GameUserEvent {
    private String gameId;
    private String userId;
    private String tableId;
    private String nickName;
    private String avatarUrl;
    private String gender;

    public GameUserDelaySignEvent() {
    }
    public GameUserDelaySignEvent(String gameId, String userId, String nickName,
                                  String avatarUrl, String gender) {
        this.gameId = gameId;
        this.userId = userId;
        this.nickName = nickName;
        this.avatarUrl = avatarUrl;
        this.gender = gender;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    @Override
    public String event() {
        return "delaySign";
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
