package com.tiantian.mtt.akka.event;

import java.io.Serializable;

/**
 *
 */
public interface Event extends Serializable {
    String event();
}
