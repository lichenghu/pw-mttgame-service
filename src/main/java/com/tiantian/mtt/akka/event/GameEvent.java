package com.tiantian.mtt.akka.event;

/**
 *
 */
public interface GameEvent extends Event {
    String gameId();
}
