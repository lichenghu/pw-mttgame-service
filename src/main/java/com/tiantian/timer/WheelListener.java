package com.tiantian.timer;

import java.util.List;

/**
 *
 */
public interface WheelListener<E> {
    void wheelElements(List<E> elments);
}
