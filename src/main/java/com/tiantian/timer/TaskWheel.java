package com.tiantian.timer;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class TaskWheel <E extends Serializable> {
    private PriorityQueue<Long> priorityQueue = new PriorityQueue<>(8192);
    private Map<Long, List<E>> taskMap = new HashMap<>();
    private WheelListener<E> wheelListener;
    private final Object lock = new Object();
    private AtomicBoolean isStop = new AtomicBoolean(true);
    private final ExecutorService service;

    public TaskWheel(WheelListener<E> wheelListener) {
        this(wheelListener, Runtime.getRuntime().availableProcessors() * 2);
    }

    public TaskWheel(WheelListener<E> wheelListener, int threadPoolSize) {
        this.wheelListener = wheelListener;
        service = Executors.newFixedThreadPool(Math.max(threadPoolSize, 1));
    }

    public void start() {
       if (isStop.compareAndSet(true, false)) {
           new Thread(new Work()).start();
        }
    }

    public void stop() {
        isStop.set(true);
    }

    public void offerTask(E task, long delayMills) {
        long time = System.currentTimeMillis() + delayMills;
        synchronized (lock) {
            List<E> tasks = taskMap.get(time);
            if(tasks == null) {
                priorityQueue.add(time);
                tasks = new ArrayList<>();
            }
            tasks.add(task);
            taskMap.put(time, tasks);
        }
    }

    public List<E> popTasks() {
           synchronized (lock) {
               long currentTimeMillis = System.currentTimeMillis();
               List<E> list = new ArrayList<>();
               for (;;) {
                    Long time = priorityQueue.poll();
                    if (time == null) {
                        return list;
                    }
                    if (currentTimeMillis < time) {
                        priorityQueue.add(time);
                        return list;
                    }
                    List<E> tasks = taskMap.remove(time);
                    if (tasks != null && tasks.size() > 0) {
                        list.addAll(tasks);
                    }
               }
           }
    }

    private class Work implements Runnable {
        @Override
        public void run() {
            for (;;) {
                if (isStop.get()) {
                    return;
                }
                List<E> tasks = popTasks();
                if (tasks != null && tasks.size() > 0) {
                    service.execute(() -> wheelListener.wheelElements(tasks));
                }
                try {
                    Thread.sleep(50);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
