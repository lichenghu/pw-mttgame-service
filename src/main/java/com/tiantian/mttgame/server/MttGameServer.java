package com.tiantian.mttgame.server;


import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.mttgame.akka.ClusterActorManager;
import com.tiantian.mttgame.data.mongodb.MGDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class MttGameServer {
    private static Logger LOG = LoggerFactory.getLogger(MttGameServer.class);
    public static void main(String[] args) {
        ClusterActorManager.init("4870");
        ClusterActorManager.init("4871");
        MGDatabase.getInstance().init();
    }

}
