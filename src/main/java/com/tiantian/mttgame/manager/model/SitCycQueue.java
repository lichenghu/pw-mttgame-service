package com.tiantian.mttgame.manager.model;

/**
 *
 */
public class SitCycQueue {
    private int maxSize;
    private int size;
    private int[] queArray;
    private int front;
    private int rear;
    private int buttonIndex = 0; // button位 默认为第一个

    public SitCycQueue(int s){
        this.maxSize = s;
        this.queArray = new int[maxSize];
        this.front = 0;
        this.rear = -1;
        this.size = 0;
    }

    public void addFront(int j) {
        addFront(j, false);
    }

    //从对头开始增加
    public void addFront(int j, boolean isButton){
        if(front == 0){
           front = maxSize;
        }
        queArray[--front] = j;
        if (isButton) {
            buttonIndex = rear;
        }
        size++;
    }
    public void addRear(int j){
        addRear(j, false);
    }
    //从尾部开始增加
    public void addRear(int j, boolean isButton){
        if(rear == maxSize - 1){
            rear = -1;
        }
        queArray[++rear] = j;
        if (isButton) {
            buttonIndex = rear;
        }
        size++;
    }

    //删除头部第一个
    public int removeFront(){
        int temp = queArray[front++];
        if(front == maxSize){
            front = 0;
        }
        size--;
        return temp;
    }

    //删除尾部最后一个
    public int removeRear(){
        int temp = queArray[rear--];
        if((rear == -1)){
            rear = maxSize - 1;
        }
        size--;
        return temp;
    }

    public boolean setButtonIndexOfValue(int val) {
        if (queArray == null) {
            return false;
        }
        for (int i = 0; i < (queArray.length - 1); i++) {
             if (queArray[i] == val) {
                 buttonIndex = i;
                 return true;
             }
        }
        return false;
    }

    public int[] returnAllByButton() {
        int[] array = new int[maxSize];
        int tmp = 0;
        for (int i = 0; i < array.length; i++) {
             int index = i + buttonIndex;
             if (index > (queArray.length - 1)) {
                 index = tmp;
                 tmp ++;
             }
             array[i] = queArray[index];
        }
        return array;
    }

    public boolean inEmpty(){
        return size == 0;
    }

    public boolean ifFull(){
        return size == maxSize;
    }
    public long peet(){
        return queArray[front];
    }

    public int size(){
        return this.size;
    }
}
