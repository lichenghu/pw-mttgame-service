package com.tiantian.mttgame.manager.model;

/**
 *
 */
public class AllinInf {
    private String userId;
    private String sitNum;
    private int index; // allin的先后顺序
    private long allChips; // allin的总筹码量

    public String getSitNum() {
        return sitNum;
    }

    public void setSitNum(String sitNum) {
        this.sitNum = sitNum;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public long getAllChips() {
        return allChips;
    }

    public void setAllChips(long allChips) {
        this.allChips = allChips;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
