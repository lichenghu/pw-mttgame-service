package com.tiantian.mttgame.manager.model;

import com.tiantian.mttgame.data.redis.RedisUtil;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import org.apache.commons.lang.StringUtils;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class TableUser extends ModeBase {
    @FieldIgnore
    private static Map<String, Field> filedMap =  new ConcurrentHashMap<>();
    @FieldIgnore
    private static AtomicBoolean LOAD_FLAG = new AtomicBoolean(false);
    @FieldIgnore
    private String userId;
    private String roomId;
    private String nickName;
    private String avatarUrl;
    private String gender;
    private String tableId;
    private String status; // gaming, standing, exit, wait
    private String betStatus;
    private String sitNum;
    private String operateCount;
    private String notOperateCount;
    // 1是亮牌
    private String showCards;
    // 上次更新的操作时间戳
    private String lastUpdateTimes;
    // 玩家下注思考时间
    private String totalSecs;
    private String tableIndex;
    private String ranking;
    private String isNewJoin; // 是不是最新加入游戏 1是  其他否

    private TableUser() {
        super();
    }

    public Map<String, Field> fieldMap(){
        return filedMap;
    }
    protected boolean hasLoad(){
        return LOAD_FLAG.compareAndSet(false, true);
    }
    public static TableUser load(String userId, String tableId) {
        TableUser tableUser = new TableUser();
        tableUser.userId = userId;
        tableUser.tableId = tableId;
        boolean ret =  tableUser.loadModel();
        if (!ret) { // 已经删除了
            return new TableUser();
        }
        return tableUser;
    }
    public static TableUser init(String userId, String tableId) {
        TableUser tableUser = new TableUser();
        tableUser.userId = userId;
        tableUser.tableId = tableId;
        return tableUser;
    }
    public boolean isNull() {
        return StringUtils.isBlank(tableId);
    }


    public boolean save() {
        if (tableId == null) {
            throw new RuntimeException("tableId can not be null");
        }
       return super.save();
    }

    @Override
    public String key() {
        return GameConstants.USER_SPINGO_TABLE_KEY + userId + ":" + tableId;
    }

    public void delSelf(String inTableId) {
       if (inTableId != null && inTableId.equalsIgnoreCase(this.tableId)) {
           RedisUtil.del(key());
       }
    }

    public void forceStandUp() {
        setSitNum(null);
        setStatus(GameConstants.USER_STATUS_STANDING);
        setBetStatus(GameEventType.FOLD);
        setOperateCount("");
        setRanking("");
        setNotOperateCount("");
        setLastUpdateTimes(System.currentTimeMillis() + "");
        save();
    }

    public boolean isExit() {
        return GameConstants.USER_STATUS_EXIT.equalsIgnoreCase(status);
    }

    public boolean isHang() {
        return GameConstants.USER_STATUS_HANG.equalsIgnoreCase(status);
    }

    public boolean isGaming() {
        return GameConstants.USER_STATUS_GAMING.equalsIgnoreCase(status);
    }

    public boolean isWaiting() {
        return GameConstants.USER_STATUS_WAIT.equalsIgnoreCase(status);
    }

    public boolean isNewJoin() {
        return StringUtils.isNotBlank(isNewJoin) && "1".equals(isNewJoin);
    }

    public void addOneOperateCount() {
        if (StringUtils.isBlank(operateCount)) {
            operateCount = "1";
        }
        operateCount = (Integer.parseInt(operateCount) + 1) + "";
    }

    public void clearNotOperateCount() {
        notOperateCount = "0";
        // 重置15秒
        totalSecs = GameConstants.BET_DELAYER_TIME / 1000 + "";
    }

    public String getTableId() {
        return tableId;
    }

//    public void setTableId(String tableId) {
//        this.tableId = tableId;
//     }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
     }

    public String getSitNum() {
        return sitNum;
    }

    public void setSitNum(String sitNum) {
        this.sitNum = sitNum;
     }

    public String getOperateCount() {
        return operateCount;
    }

    public void setOperateCount(String operateCount) {
        this.operateCount = operateCount;
     }

    public String getBetStatus() {
        return betStatus;
    }

    public void setBetStatus(String betStatus) {
        this.betStatus = betStatus;
     }

    public String getNotOperateCount() {
        return notOperateCount;
    }

    public void setNotOperateCount(String notOperateCount) {
        this.notOperateCount = notOperateCount;
     }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
     }

    public String getLastUpdateTimes() {
        return lastUpdateTimes;
    }

    public void setLastUpdateTimes(String lastUpdateTimes) {
        this.lastUpdateTimes = lastUpdateTimes;
    }

    public String getShowCards() {
        return showCards;
    }

    public void setShowCards(String showCards) {
        this.showCards = showCards;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getTotalSecs() {
        if (StringUtils.isBlank(totalSecs)) {
            System.out.println("====totalSecs is blank=======");
            return GameConstants.BET_DELAYER_TIME / 1000 + "";
        }
        return totalSecs;
    }

    public void setTotalSecs(String totalSecs) {
        if (StringUtils.isBlank(totalSecs)) {
            System.out.println("===set=totalSecs is blank=======");
        }
        this.totalSecs = totalSecs;
    }

    public String getTableIndex() {
        return tableIndex;
    }

    public void setTableIndex(String tableIndex) {
        this.tableIndex = tableIndex;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getIsNewJoin() {
        return isNewJoin;
    }

    public void setIsNewJoin(String isNewJoin) {
        this.isNewJoin = isNewJoin;
    }
}
