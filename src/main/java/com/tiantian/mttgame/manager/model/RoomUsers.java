package com.tiantian.mttgame.manager.model;

import com.tiantian.mttgame.data.redis.RedisUtil;
import com.tiantian.mttgame.manager.constants.GameConstants;

/**
 *
 */
public class RoomUsers {
    public static void addOnlineUsers(String roomId, String userId) {
       boolean isMem = RedisUtil.sismember(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
       if (!isMem) {
           RedisUtil.sadd(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
       }
    }

    public static void removeOnlineUsers(String roomId, String userId) {
        RedisUtil.srem(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
    }
}
