package com.tiantian.mttgame.manager.model;

import com.tiantian.mttgame.data.redis.RedisUtil;
import com.tiantian.mttgame.manager.constants.GameConstants;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class UserGames {
    private String userId;

    private String currentGameId;

    private List<String> gameIds;

    public static UserGames load(String userId) {
        UserGames userGames = new UserGames();
        userGames.userId = userId;
        // key gameId, value 1 是当前房间, 0 不是当前房间
        Map<String, String> gameMap = RedisUtil.getMap(GameConstants.SPINGO_USER_GAMES_KEY + userId);
        userGames.gameIds = new ArrayList<>();
        if (gameMap != null) {
            for (Map.Entry<String, String> entry : gameMap.entrySet()) {
                 String gameId = entry.getKey();
                 String isCurrent = entry.getValue();
                 userGames.gameIds.add(gameId);
                 if ("1".equalsIgnoreCase(isCurrent)) {
                     userGames.currentGameId = gameId;
                 }
            }
        }
        return userGames;
    }

    /**
     * 加入到当前的游戏中
     * @param joinGameId
     */
    public void joinCurrentGame(String joinGameId) {
        if (StringUtils.isBlank(joinGameId)) {
            return;
        }
        if (!joinGameId.equalsIgnoreCase(currentGameId)) {
            if (StringUtils.isNotBlank(currentGameId)) {
                RedisUtil.setMap(GameConstants.SPINGO_USER_GAMES_KEY + userId, currentGameId, "0");
            }
            RedisUtil.setMap(GameConstants.SPINGO_USER_GAMES_KEY + userId, joinGameId, "1");
        }
    }

    /**
     * 删除该游戏
     * @param gameId
     */
    public void deleteGameId(String gameId) {
        if (StringUtils.isBlank(gameId)) {
            return;
        }
        RedisUtil.delMapField(GameConstants.SPINGO_USER_GAMES_KEY + userId, gameId);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCurrentGameId() {
        return currentGameId;
    }

    public void setCurrentGameId(String currentGameId) {
        this.currentGameId = currentGameId;
    }

    public List<String> getGameIds() {
        return gameIds;
    }

    public void setGameIds(List<String> gameIds) {
        this.gameIds = gameIds;
    }
}
