package com.tiantian.mttgame.manager.constants;
/**
 *
 */
public enum GameStatus {
    INIT, //  初始化状态
    READY, // 结束后准备状态
    BEGIN(),
    D_AND_B, //  庄家和大小盲注
    PRE_FLOP, //
    FLOP, //
    TURN, //
    RIVER, //
    FINISH,
    END; //玩家可能全部弃牌，直接强退


    GameStatus() {
    }

    public static GameStatus next(String name) {
        GameStatus gameStatus = GameStatus.valueOf(name);
        int ordinal = gameStatus.ordinal();
        int nextOrdinal = ordinal + 1;
        if (nextOrdinal >= values().length) {
            return null;
        }
        return values()[nextOrdinal];
    }
}
