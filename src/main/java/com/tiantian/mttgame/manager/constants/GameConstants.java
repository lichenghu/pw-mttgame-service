package com.tiantian.mttgame.manager.constants;

/**
 *
 */
public interface GameConstants {
    String TASK_EVENT = "mttTaskEvent";
    long INIT_TO_BEGIN_DELAYER_TIME = 30000;

    long BEGIN_DELAYER_TIME_SECS = 1000;

    long  DANDB_DELAYER_TIME = 3000;
    // 下注的超时时间
    long  BET_DELAYER_TIME = 5000;
//    // 下注的超时时间
//    long  BET_DELAYER_TIME = 25000;
    // 桌子状态超时时间
    long TABLE_TIMEOUT_MILL = 60000;
    //  下一个状态任务延时
    long STATUS_DELAYER_TIME = 700;

    // 桌子状态检查延迟任务
    long TABLE_STATUS_DELAYER_TIME = 10000;

    // 桌子状态玩家筹码后开始dandb延迟任务
    long AFTER_CHECK_CHIPS_DELAYER_TIME =  0; // 1000;

    // 桌子上在线玩家状态检查延迟任务
    long TABLE_ONLINE_USER_DELAYER_TIME = 120000;
    // 玩家买入剩余时间
    long USER_REBUY_TIME = 15000;

    int MAX_NOT_OPERATE_COUNT = 2;
    String SPINGO_ROOM_ONLINE_USERS_KEY = "mtt_room_online_users_status:";
    String SPINGO_TABLE_STATUS_KEY = "mtt_table_status:";
    // 局桌子玩家
    String SPINGO_TABLE_USER_KEY = "mtt_table_user_key:";
    String ROUTER_KEY = "router_key:";
    // 桌子上玩家
    String USER_SPINGO_TABLE_KEY = "user_mtt_table_key:";
    // 桌子在线玩家
     String SPINGO_TABLE_USER_ONLINE_KEY = "mtt_table_user_online_key:";
    // 局桌子坐下过的玩家
    //String SPINGO_TABLE_USER_SIT_DOWN_KEY = "sng_table_user_sit_down_key:";

    String USER_SPINGO_GAME_KEY = "user_mtt_games_key:";
    String SPINGO_USER_GAMES_KEY = "mtt_user_games_key:";
    // map key
    String DATA_STR = "dataStr";
    String EVENT = "event";
    String SERVER_ID = "serverId";
    String TO = "to";
    // 消息来源
    String MSG_SOURCE = "msg_source";
    String UNIQUE_ID = "unique_id";

    // 事件
    String TABLE_START = "tableStart";
    String USER_JOIN = "userJoin";
    String USER_RAISE = "userRaise";
    String USER_FOLD = "userFold";
    String USER_ALLIN = "userAllin";
    String USER_CHECK = "userCheck";
    String USER_STAND_UP = "userStandUp";
    String USER_CALL = "userCall";
    String USER_EXIT = "exit";
    String USER_REJOIN = "userRejoin";
    String USER_FAST_RAISE = "userFastRaise";
    String SHOW_CARDS = "showCards";
    String ADD_TIMES = "addTimes"; // 蓄时
    String TABLE_INFO = "tableInfo";

    String ADD_USER = "addUser";
    String MOVE_USER = "moveUser";

    String UPGRADE_BLIND = "upgradeBlind";

    // 任务
    String TEST_BET = "testBet"; // 检测下注
    //
    String TEST_STATUS = "testStatus"; // 检测下个任务

    //
    String TEST_LAST_USER = "testLastUer"; // 检测桌子上最后一个玩家

    //
    String TABLE_STATUS_CHECK = "tableStatusCheck";

    // 检测桌子上在线玩家的状态，掉线需要清楚掉
    String TABLE_ONLINE_USER_CHECK = "tableOnlineUserCheck";

    // 玩家状态
    String USER_STATUS_GAMING = "gaming";
    // 玩家状态
    String USER_STATUS_WAIT = "wait";

    String USER_STATUS_EXIT = "exit";

    String USER_STATUS_STANDING = "standing";

    String USER_STATUS_HANG = "hang";

    String USER_STATUS_WAIT_REBUY = "wait_rebuy";

    // 筹码校验
    String CHECK_CHIPS = "checkChips";


    String USER_ENTER = "gameUserEnter";

    String UPDATE_RANKING = "updateRanking";

    String USER_REBUY = "userRebuy";

    String USER_REBUY_TASK = "userRebuyTask";

    String UPDATE_TOTAL_CNT = "updateTotalUsers";

    String USER_RETURN_SIT = "returnSit";

    String TABLE_CLEAR = "tableClear";

    String TABLE_REST = "tableRest";
}
