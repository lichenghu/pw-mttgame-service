package com.tiantian.mttgame.manager.texas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jeffma on 15/8/26.
 */
public class PokerRecognizer {
    static Logger LOG = LoggerFactory.getLogger(PokerRecognizer.class);
    private List<Poker> pokerList;
    private List<Poker> spadeList;
    private List<Poker> heartList;
    private List<Poker> clubList;
    private List<Poker> diamondList;
    private List<PokerPairs> pairsList;

    public List<PokerOuts> getOutsList() {
        return outsList;
    }

    public PokerOuts getMaxOuts(){
        PokerOuts outs = null;
        if(outsList.isEmpty()==false){
            outs = outsList.get(0);
        }
        return outs;
    }

    private List<PokerOuts> outsList;

    public List<PokerPairs> getPairsList() {
        return pairsList;
    }
    public List<Poker> getPokerList() {
        return pokerList;
    }
    public List<Poker> getSpadeList() {
        return spadeList;
    }

    public List<Poker> getHeartList() {
        return heartList;
    }

    public List<Poker> getClubList() {
        return clubList;
    }

    public List<Poker> getDiamondList() {
        return diamondList;
    }
    public void load(List<Poker> pokers){
        clearPokers();
        for(Poker p:pokers){
            Poker cp = new Poker(p);
            pokerList.add(cp);
            //split into suits
            switch (p.getSuit()){
                case "spade":
                    spadeList.add(p);
                    break;
                case "heart":
                    heartList.add(p);
                    break;
                case "club":
                    clubList.add(p);
                    break;
                case "diamond":
                    diamondList.add(p);
                    break;
                default:
                    break;
            }
            //split into pairs
            addPokerToPairsList(p);

        }
        sortPokers(pokerList);
        sortPokers(spadeList);
        sortPokers(heartList);
        sortPokers(clubList);
        sortPokers(diamondList);
        sortPairs(pairsList);
        findFlush();
        findStraight();
        int maxPairsSize = getMaxPairsSize();
        if(maxPairsSize==4){
            findFourOfKind();
        }else if(maxPairsSize==3){
            findThreeOrFullHouse();
        }else if(maxPairsSize==2){
            findPairs();
        }else{
            findHighCards();
        }
        sortOuts(outsList);
    }
    public void sortPokers(List<Poker> pokers){
        Collections.sort(pokers);
        Collections.reverse(pokers);
    }
    public void sortPairs(List<PokerPairs> pairs){
        Collections.sort(pairs);
        Collections.reverse(pairs);
    }
    public void sortOuts(List<PokerOuts> outs){
        Collections.sort(outs);
        Collections.reverse(outs);
    }
    public void clearPokers(){
        pokerList.clear();
        spadeList.clear();
        heartList.clear();
        clubList.clear();
        diamondList.clear();
        pairsList.clear();
        outsList.clear();
    }
    public void addPokerToPairsList(Poker poker){
        boolean isNewPoker = true;
        // find point in pairs
        for(PokerPairs pairs:pairsList){
            if(pairs.getNumber()==poker.getNumber()){
                isNewPoker = false;
                pairs.add(poker);
                break;
            }
        }
        // if new point,create a pair,and add it to pair
        if(isNewPoker){
            PokerPairs tmpPairs =  new PokerPairs(poker.getNumber());
            tmpPairs.add(poker);
            pairsList.add(tmpPairs);
        }
    }

    private void findFlush(){
        List<Poker> suits = null;
        List<Poker> tmpList = new ArrayList<>();
        PokerOuts tmpOuts =  new PokerOuts();
        List<Poker> tmpStraightFlushList = new ArrayList<>();
        PokerOuts tmpStraightFlushOuts =  new PokerOuts();
        if(spadeList.size()>=5){
            suits =  spadeList;
        }else if(heartList.size()>=5){
            suits = heartList;
        }else if(clubList.size()>=5){
            suits = clubList;
        }else if(diamondList.size()>=5){
            suits = diamondList;
        }
        // find flush
        if(null!=suits){
            for(int i=0;i<5;i++){
                tmpList.add(suits.get(i));
            }
            tmpOuts.setOutList(tmpList);
            // 5 is flush level
            tmpOuts.setLevel(5);
            outsList.add(tmpOuts);
        }
        // find straight flush
        if(null!=suits){
            int suitsLength = suits.size();
            int maxNumber = suits.get(0).getNumber();
            int fourthMinNumber = 0;
            if(suitsLength>=5){
                fourthMinNumber = suits.get(suitsLength-4).getNumber();
            }
            // find straight flush but A 2 3 4 5
            for(int i=0;i<=suitsLength-5;i++){
                int highNumber = suits.get(i).getNumber();
                int lowNumber = suits.get(i+4).getNumber();
                if((highNumber-lowNumber)==4){
                    // is straight
                    for(int j=i;j<i+5;j++){
                        tmpStraightFlushList.add(suits.get(j));
                    }
                    break;
                }
            }
            if(tmpStraightFlushList.isEmpty()==true) {
                if(maxNumber==14 && fourthMinNumber==5){
                    // is  A * 5 4 3 2
                    for(int i=suitsLength-4;i<suitsLength;i++){
                        tmpStraightFlushList.add(suits.get(i));
                    }
                    tmpStraightFlushList.add(suits.get(0));
                }
            }
            if(tmpStraightFlushList.isEmpty()==false){
                tmpStraightFlushOuts.setOutList(tmpStraightFlushList);
                // 8 is straight flush level,9 is royal flush level
                if(tmpStraightFlushList.get(0).getNumber()==14)
                {
                    tmpStraightFlushOuts.setLevel(9);
                }else{
                    tmpStraightFlushOuts.setLevel(8);
                }
                outsList.add(tmpStraightFlushOuts);
            }
        }
    }
    private void findStraight(){
        PokerOuts tmpOuts = null;
        List<Poker> tmpList = new ArrayList<>();
        int pairsLength = pairsList.size();
        int maxNumber = pairsList.get(0).getNumber();

        int fourthMinNumber = 0;
        if(pairsLength>=5){
            fourthMinNumber = pairsList.get(pairsLength-4).getNumber();
        }
        //find straight but A 5 4 3 2
        for(int i=0;i<=pairsLength-5;i++){
            int highNumber = pairsList.get(i).getNumber();
            int lowNumber = pairsList.get(i+4).getNumber();
            if((highNumber-lowNumber)==4){
                // is straight
                for(int j=i;j<i+5;j++){
                    tmpList.add(pairsList.get(j).getPokerList().get(0));
                }
                break;
            }
        }
        // find  A * 5 4 3 2
        if(tmpList.isEmpty()==true)
        {
            if(maxNumber==14 && fourthMinNumber==5){
                // is  A * 5 4 3 2
                for(int i=pairsLength-4;i<pairsLength;i++){
                    tmpList.add(pairsList.get(i).getPokerList().get(0));
                }
                tmpList.add(pairsList.get(0).getPokerList().get(0));

            }
        }
        if(tmpList.isEmpty()==false){
            tmpOuts = new PokerOuts();
            tmpOuts.setOutList(tmpList);
            // 4 is flush level
            tmpOuts.setLevel(4);
            outsList.add(tmpOuts);
        }
    }

    private void findFourOfKind(){
        List<Poker> tmpList = new ArrayList<>();
        PokerOuts tmpOuts = null;;
        int number = 0;
        for(PokerPairs pairs:pairsList){
            if(pairs.getPokerList().size()==4){
                for(Poker p:pairs.getPokerList()){
                    tmpList.add(p);
                }
                number = pairs.getNumber();
                break;
            }
        }
        if(number!=0){
            for(PokerPairs pairs:pairsList){
                if(pairs.getNumber()!=number){
                    tmpList.add(pairs.getPokerList().get(0));
                    break;
                }
            }
        }
        if(tmpList.isEmpty()==false){
            tmpOuts = new PokerOuts();
            tmpOuts.setOutList(tmpList);
            // 7 is four of a kind
            tmpOuts.setLevel(7);
            outsList.add(tmpOuts);
        }
    }
    private void findThreeOrFullHouse(){
        int threeNumber = 0;
        int twoNumber = 0;
        List<Poker> tmpList = new ArrayList<>();
        PokerOuts tmpOuts = new PokerOuts();
        // find three
        for(PokerPairs pairs:pairsList){
            if(pairs.getPokerList().size()==3){
                for(Poker p:pairs.getPokerList()){
                    tmpList.add(p);
                }
                threeNumber = pairs.getNumber();
                break;
            }
        }
        // find two
        for(PokerPairs pairs:pairsList){
            if(pairs.getPokerList().size()>=2 && pairs.getNumber()!=threeNumber){
                for(int i=0;i<=1;i++){
                    tmpList.add(pairs.getPokerList().get(i));
                }
                twoNumber = pairs.getNumber();
                break;
            }
        }
        // if two number is 0 , it's a three of a kind
        if(twoNumber==0){
            for(Poker p:pokerList){
                if(tmpList.size()<5 && p.getNumber()!=threeNumber){
                    tmpList.add(p);
                }
            }
        }
        tmpOuts.setOutList(tmpList);
        outsList.add(tmpOuts);
        //when three number andd two number is > 0 is full house
        if(twoNumber>0){
            tmpOuts.setLevel(6);
        }else if(twoNumber==0) {
            tmpOuts.setLevel(3);
        }
    }
    private void findPairs(){
        List<Poker> tmpList = new ArrayList<>();
        PokerOuts tmpOuts = new PokerOuts();
        int firstPairNumber = 0;
        int secondPairNumber = 0;
        // find first pair
        for(PokerPairs pairs:pairsList){
            if(pairs.getPokerList().size()==2){
                for(Poker p:pairs.getPokerList()){
                    tmpList.add(p);
                }
                firstPairNumber = pairs.getNumber();
                break;
            }
        }
        // find second pair
        for(PokerPairs pairs:pairsList){
            if(pairs.getPokerList().size()==2 && pairs.getNumber()!=firstPairNumber){
                for(Poker p:pairs.getPokerList()){
                    tmpList.add(p);
                }
                secondPairNumber = pairs.getNumber();
                break;
            }
        }
        if(secondPairNumber==0){
            for(Poker p:pokerList){
                if(tmpList.size()<5 && p.getNumber()!=firstPairNumber){
                    tmpList.add(p);
                }
            }
        }else{
            for(Poker p:pokerList){
                if(tmpList.size()<5 && p.getNumber()!=firstPairNumber && p.getNumber()!=secondPairNumber){
                    tmpList.add(p);
                }
            }
        }
        tmpOuts.setOutList(tmpList);
        outsList.add(tmpOuts);
        //when three number andd two number is > 0 is full house
        if(secondPairNumber>0){
            tmpOuts.setLevel(2);
        }else if(secondPairNumber==0) {
            tmpOuts.setLevel(1);
        }
    }
    private void findHighCards(){
        List<Poker> tmpList = new ArrayList<>();
        PokerOuts tmpOuts = new PokerOuts();
        for(Poker p:pokerList){
            if(tmpList.size()<5){
                tmpList.add(p);
            }
        }
        tmpOuts.setOutList(tmpList);
        tmpOuts.setLevel(0);
        outsList.add(tmpOuts);
    }
    private int getMaxPairsSize(){
        int size = 0;
        for(PokerPairs pairs:pairsList){
            if (pairs.getPokerList().size()>size){
                size = pairs.getPokerList().size();
            }
        }
        return size;
    }

    public PokerRecognizer(){
        pokerList = new ArrayList();
        spadeList = new ArrayList();
        heartList = new ArrayList();
        clubList = new ArrayList();
        diamondList = new ArrayList();
        pairsList = new ArrayList<>();
        outsList = new ArrayList<>();
    }
}
