package com.tiantian.mttgame.manager.texas;

import java.util.List;

/**
 * Created by jeffma on 15/8/29.
 */
public class PokerOuts implements Comparable {
    private List<Poker> outList;
    private int value;
    // 0: high card,1: one pair,2: two pair,3: three of a kind,4:straight
    // 5: flush,6:full house,7:four of a kind,8:straight flush,9:royal flush
    private int level;

    public long getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getValue() {
        int outSize = outList.size();
        value = (level << 20) & 0x00f00000;
        for (int i = 0; i < outSize; i++) {
            int pValue = outList.get(i).getNumber() << ((4 - i) * 4);
            value = value | pValue;
        }
        return value;
    }

    public List<Poker> getOutList() {
        return outList;
    }

    public void setOutList(List<Poker> outList) {
        this.outList = outList;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof PokerOuts) {
            PokerOuts outs = (PokerOuts) o;
            if (this.getValue() > outs.getValue()) {
                return 1;
            } else if (this.getValue() == outs.getValue()) {
                return 0;
            } else {
                return -1;
            }
        }
        return -1;
    }

    public void PokerOuts() {
        this.level = 0;
        this.value = 0;
        this.outList = null;
    }
}
