package com.tiantian.mttgame.manager.texas;


import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeffma on 15/8/28.
 */
public class PokerPairs implements Comparable{
    private int number;
    private List<Poker> pokerList;
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Poker> getPokerList() {
        return pokerList;
    }

    public PokerPairs(){
        number = 0;
        pokerList =  new ArrayList();
    }
    public PokerPairs(int number){
        this.setNumber(number);
        pokerList =  new ArrayList();
    }
    public boolean add(Poker poker){
        boolean result = true;
        if(0==number){
            this.setNumber(poker.getNumber());
        }else if(number==poker.getNumber()){
            pokerList.add(poker);
        }else{
            result = false;
        }
        return result;
    }
    @Override
    public int compareTo(Object o) {
        if(o instanceof PokerPairs){
            PokerPairs pairs=(PokerPairs)o;
            if(this.number>pairs.number){
                return 1;
            }
            else if(this.number==pairs.number){
                return 0;
            }else{
                return -1;
            }
        }
        return -1;
    }
    public String toString(){
        return StringUtils.join(pokerList, " ");
    }
}
