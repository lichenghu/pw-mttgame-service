package com.tiantian.mttgame.manager.texas;

/**
 * Created by jeffma on 15/8/26.
 */
public class Poker implements Comparable{
    private int number;
    private String suit; //spade,heart,club,diamond

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getSuit() {
        return suit;
    }

    public void setSuit(String suit) {
        this.suit = suit;
    }
    public Poker(){

    }
    public Poker(int number,String suit){
        this.setNumber(number);
        this.setSuit(suit);
    }

    public Poker(Poker p){
        this.setNumber(p.getNumber());
        this.setSuit(p.getSuit());
    }

    // 从2 - 53 张牌中选
    public static Poker create(int num) {
        int mod = num % 4;
        String suit = "";
        if (mod == 2) {
            suit = "spade";
        } else if(mod == 3) {
            suit = "heart";
        } else if(mod == 0) {
            suit = "club";
        } else {
            suit = "diamond";
        }

        int newNum = num + 6;
        int value = newNum / 4;
        return new Poker(value, suit);
    }

    public static Poker create(String shortStr) {
        if (shortStr == null) {
            return null;
        }
        if (shortStr.length() < 2) {
            return null;
        }
        String suitShort = shortStr.substring(0 ,1);
        String number = shortStr.substring(1);
        String suitStr = "";
        if (suitShort.equalsIgnoreCase("s")) {
            suitStr = "spade";
        }
        if (suitShort.equalsIgnoreCase("h")) {
            suitStr = "heart";
        }
        if (suitShort.equalsIgnoreCase("c")) {
            suitStr = "club";
        }
        if (suitShort.equalsIgnoreCase("d")) {
            suitStr = "diamond";
        }
        return new Poker(Integer.parseInt(number), suitStr);
    }

    public String getShortPoker() {
       String val = suit.substring(0, 1).toUpperCase() + number;
       return val;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Poker){
            Poker p=(Poker)o;
            if(this.number>p.number){
                return 1;
            }
            else if(this.number==p.number){
                return 0;
            }else{
                return -1;
            }
        }
        return -1;
    }
    public String toString(){
        return this.suit + " " + this.number;
    }
}
