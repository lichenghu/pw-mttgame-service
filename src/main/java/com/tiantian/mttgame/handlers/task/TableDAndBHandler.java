package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.Handlers;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.*;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import java.util.*;

/**
 *
 */
public class TableDAndBHandler implements EventHandler<TableTaskEvent> {

    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject params = event.getParams();
        String tableId = event.tableId();
        String randomBtn = params.getString("randomBtn");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        String oldButton = tableStatus.getButton();
        if (StringUtils.isNotBlank(randomBtn)) {
            oldButton = randomBtn;
        }

        // 更新等待状态的玩家
        checkWaitStatus(tableId, tableStatus.getRoomId());

        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);

        // 包括gaming和exit状态的玩家
        Map<String, String> userGamingExitHang = tableAllUser.userGamingExitHangSitUserId();
        String[] results = getButtonAndBlind(userGamingExitHang.keySet(), oldButton,
                tableAllUser.getOldSitDownUserMap().keySet());
        String button = results[0];
        String smallBlindNum = results[1];
        String bigBlindNum = results[2];
        String inningId = UUID.randomUUID().toString().replace("-", "");
        // 设置牌局ID
        tableStatus.setInningId(inningId);
        // 设置庄家,小,大盲注
        tableStatus.setButton(button);
        tableStatus.setSmallBlindNum(smallBlindNum);
        tableStatus.setBigBlindNum(bigBlindNum);


        tableStatus.setStatus(GameStatus.D_AND_B.name());
        // 添加可以参加游戏的玩家座位号
        tableStatus.addPlayingSits(userGamingExitHang.keySet());
        tableStatus.addPlayingSitsAndId(userGamingExitHang);

        tableStatus.setCurrentBet(bigBlindNum);
        tableStatus.setSitBetStatusNotFlush(tableStatus.getBigBlindNum(), GameEventType.CALL); // 大盲注默认为call
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");

        // 初始化 玩家的下注日志
        List<UserBetLog> logList = new ArrayList<>();
        Set<Map.Entry<String, String>> joinTableUserMap = userGamingExitHang.entrySet();
        //System.out.println("JSONTableUser:" + JSON.toJSONString(joinTableUserMap));
        for (Map.Entry<String, String> entry : joinTableUserMap) {
            String sitNum = entry.getKey();
            String userId = entry.getValue();
            UserBetLog userBetLog = new UserBetLog();
            userBetLog.setUserId(userId);
            userBetLog.setSitNum(sitNum);
            userBetLog.setRoundChips(0);
            userBetLog.setTotalChips(0);
            logList.add(userBetLog);
        }
        tableStatus.setUsersBetsLog(JSON.toJSONString(logList));
        // 清空上局的牌
        tableStatus.setCards("");
        // 清空上局玩家的底牌
        tableStatus.setUserCards("");
        // 清空上局桌面的牌
        tableStatus.setDeskCards("");

        // 设置玩家大盲注玩家状态为check
        String bigBlindUserId = userGamingExitHang.get(bigBlindNum);
        String smallBlindUserId = userGamingExitHang.get(smallBlindNum);

        TableUser tableUser = TableUser.load(bigBlindUserId, tableId);
        tableUser.setBetStatus(GameEventType.CHECK);
        tableUser.save();

        // 设置ante逻辑
        List<GameStartEvent.Rule> ruleList = tableStatus.game().getRules();
        int currentLvl = Integer.parseInt(tableStatus.getCurrentBlindLevel()); // 从1开始
        if (currentLvl > ruleList.size()) {
            currentLvl = ruleList.size();
        }
        GameStartEvent.Rule rule = ruleList.get(currentLvl - 1); // 当前盲注规则
        long ante = rule.getAnte();
        long totalAnte = 0;
        Map<String, Long> userBetAnte = new HashMap<>();
        for (Map.Entry<String, String> entry : joinTableUserMap) {
             String sitNum = entry.getKey();
             String userId = entry.getValue();
             UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
             if (userChips == null) {
                 continue;
             }
             long leftChips = userChips.getChips();
             long userAnte;
             if (leftChips >= ante) {
                 userChips.reduceAndFlushChips(ante);
                 userAnte = ante;
             } else {
                 userChips.reduceAndFlushChips(leftChips);
                 userAnte = leftChips;
             }
             totalAnte += userAnte;
             userBetAnte.put(sitNum, userAnte);
             tableStatus.addUserAnte(sitNum, userAnte);
             if (userChips.getChips() == 0 &&
                     !userId.equalsIgnoreCase(smallBlindUserId)
                        && !userId.equalsIgnoreCase(bigBlindUserId)) {// allin
                 TableUser tbUser = TableUser.load(userId, tableId);
                 tbUser.setBetStatus(GameEventType.ALLIN);
                 tbUser.save();
             }
        }

        long smallBlindMoney = Long.parseLong(tableStatus.getSmallBlindMoney());
        long bigBlindMoney = Long.parseLong(tableStatus.getBigBlindMoney());


        // 扣除玩家的大小盲注
        UserChips smallUserChips = UserChips.load(smallBlindUserId, tableStatus.getRoomId());
        long smallBlindChips = Math.min(smallUserChips.getChips(), smallBlindMoney);
        smallUserChips.reduceAndFlushChips(smallBlindChips);

        UserChips bigUserChips = UserChips.load(bigBlindUserId, tableStatus.getRoomId());
        long bigBlindChips = Math.min(bigUserChips.getChips(), bigBlindMoney);
        bigUserChips.reduceAndFlushChips(bigBlindChips);
        Long smallBetAnte = userBetAnte.get(smallBlindNum);
        tableStatus.userBet(smallBlindNum, smallBlindChips + (smallBetAnte == null ? 0 : smallBetAnte));
        Long bigBetAnte = userBetAnte.get(bigBlindNum);
        tableStatus.userBet(bigBlindNum, bigBlindChips + (bigBetAnte == null ? 0 : bigBetAnte));
        // 设置下的钱注
        for (Map.Entry<String, Long> betAnteEntry : userBetAnte.entrySet()) {
             String sit = betAnteEntry.getKey();
             Long uba = betAnteEntry.getValue();
             if (!sit.equalsIgnoreCase(smallBlindNum) && !sit.equalsIgnoreCase(bigBlindNum) ) {
                 tableStatus.userBet(sit, uba);
             }
        }

        boolean smallIsAllin = false;
        boolean bigIsAllin = false;
        Map<String, String> exitAndHangMap = tableAllUser.userGamingExitHangSitUserId();
        if (exitAndHangMap.size() > 0) {
            for (Map.Entry<String, String> entry : exitAndHangMap.entrySet()) {
                 String sitNum = entry.getKey();
                 String userId = entry.getValue();
                 checkExitAndHung(tableStatus, tableAllUser, tableId, userId, sitNum, bigBlindNum, smallBlindNum);
            }
        }

        // 判断是不是allin
        if (smallUserChips.getChips() <= 0) {
            TableUser smallBlindUser = TableUser.load(smallBlindUserId, tableId);
            smallBlindUser.setBetStatus(GameEventType.ALLIN);
            smallBlindUser.save();
            tableStatus.setSitBetStatusNotFlush(tableStatus.getSmallBlindNum(), GameEventType.ALLIN);
            smallIsAllin = true;
        } else {
            // 判断是不是exit或者hang
            if (tableAllUser.isExitOrHangByUserId(smallBlindUserId)) {
                TableUser smallBlindUser = TableUser.load(smallBlindUserId, tableId);
                smallBlindUser.setBetStatus(GameEventType.FOLD);
                smallBlindUser.save();
                tableStatus.setSitBetStatusNotFlush(tableStatus.getSmallBlindNum(), GameEventType.FOLD);
            }
        }

        // 判断是不是allin
        if (bigUserChips.getChips() <= 0) {
            TableUser bigBlindUser = TableUser.load(bigBlindUserId, tableId);
            bigBlindUser.setBetStatus(GameEventType.ALLIN);
            bigBlindUser.save();
            tableStatus.setSitBetStatusNotFlush(tableStatus.getBigBlindNum(), GameEventType.ALLIN);
            bigIsAllin = true;
        }
        tableStatus.save();

        List<Map<String, Object>> maps = new ArrayList<>();
        for (Map.Entry<String, String> entry : joinTableUserMap) {
            Map<String, Object> map = new HashMap<>();
            UserChips userChips = UserChips.load(entry.getValue(), tableStatus.getRoomId());
            if (userChips == null) {
                continue;
            }
            map.put("s", Integer.parseInt(entry.getKey()));
            map.put("m", userChips.getChips());
            maps.add(map);
        }

        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("btn", Integer.parseInt(button)); // 庄家位
        object.put("smb", Integer.parseInt(smallBlindNum)); // 小盲注座位号
        object.put("smbm", smallBlindChips); //设置测试小盲注值
        object.put("bgb", Integer.parseInt(bigBlindNum)); // 大盲注座位号
        object.put("bgbm", bigBlindChips); //设置测试大盲注
        object.put("total_ante", totalAnte);
        object.put("ante",  Long.parseLong(tableStatus.getAnte()));
        object.put("umoney", maps);
        long nextUpgradeSecs = 0;
        if (ruleList.size() > currentLvl) {
            long nextUpBlindTimes = Long.parseLong(tableStatus.getNextUpBlindTimes());
            nextUpgradeSecs = Math.max(0, (nextUpBlindTimes - System.currentTimeMillis()) / 1000);
        }
        object.put("next_upgrade_secs", nextUpgradeSecs);
        object.put("lvl", tableStatus.getCurrentBlindLevel());
        // 通知所有玩家
        sendToUsers(object, tableAllUser.getOnlineTableUserIds(), tableId, tableStatus.getRoomId());

        if (smallIsAllin) {
            sendUserAllin(tableStatus, smallBlindUserId,  0,
                    Integer.parseInt(smallBlindNum), tableAllUser.getOnlineTableUserIds());
        }

        if (bigIsAllin) {
            sendUserAllin(tableStatus, bigBlindUserId,  0,
                    Integer.parseInt(bigBlindNum), tableAllUser.getOnlineTableUserIds());
        }

        // 立即触发pre_flop
        JSONObject eventParam = new JSONObject();
        eventParam.put("tableId", tableId);
        eventParam.put("inningId", inningId);

        TableTaskEvent tableTaskEvent = new TableTaskEvent();
        tableTaskEvent.setEvent(GameStatus.PRE_FLOP.name());
        tableTaskEvent.setParams(eventParam);
        Handlers.INSTANCE.execute(tableTaskEvent, self, context, sender);

    }

    private static String[] getButtonAndBlind(Set<String> sitNumSet, String oldButton, Set<String> oldSitNumSet) {
        // button 位置
        String button = null;
        // 小盲注座位号
        String sbSitNum = null;
        // 大盲注座位号
        String bbSitNum = null;
        // 转换进行排序
        TreeSet<String> oldSitTreeSet = new TreeSet<>(oldSitNumSet);
        // 没有button位置，则座位最小号为button位
        if (StringUtils.isBlank(oldButton)) {
            button = oldSitTreeSet.first();
        } else { // 之前存在了button位置，则算出下一位button
            Iterator<String> iterator = oldSitTreeSet.iterator();
            while (iterator.hasNext()) {
                String sitNum = iterator.next();
                // 第一个比oldButton大的值
                if (Integer.parseInt(sitNum) > Integer.parseInt(oldButton)) {
                    button = sitNum;
                    break;
                }
            }
            // 如果没有找到则说明 oldButton是最大值，则设置座位队列第一个为button位
            if (button == null) {
                button = oldSitTreeSet.first();
            }
        }

        // 转换进行排序
        TreeSet<String> sitTreeSet = new TreeSet<>(sitNumSet);
        // 获取大盲注和小盲注座位号
        Iterator<String> sbIterator = sitTreeSet.iterator();
        while (sbIterator.hasNext()) {
            String sitNum = sbIterator.next();
            // 比button座位号大
            if (Integer.parseInt(sitNum) > Integer.parseInt(button)) {
                if (sbSitNum == null) {
                    sbSitNum = sitNum;
                    break;
                }
            }
        }
        // 小盲注没找到则第一个为小盲注
        if (sbSitNum == null) {
            sbSitNum = sitTreeSet.first();
        }
        // 查找大盲注
        Iterator<String> bbIterator = sitTreeSet.iterator();
        while (bbIterator.hasNext()) {
            String sitNum = bbIterator.next();
            // 比button座位号大
            if (Integer.parseInt(sitNum) > Integer.parseInt(sbSitNum)) {
                if (bbSitNum == null) {
                    bbSitNum = sitNum;
                    break;
                }
            }
        }
        // 大盲注没找到则第一个为大盲注
        if (bbSitNum == null) {
            bbSitNum = sitTreeSet.first();
        }
        if (sitTreeSet.size() == 2) {
            return new String[]{button, button, sbSitNum};
        }
        return new String[]{button, sbSitNum, bbSitNum};
    }

    private void sendToUsers(JSONObject object, Collection<String> userIds, String tableId, String gameId) {
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.DANDB, userIds, id, gameId);
    }

    private void sendUserAllin(TableStatus tableStatus, String userId, int val, int sitNum, Collection<String> userIds) {
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("uid", userId);
        object.put("sn", sitNum);
        //设置allin的数值
        object.put("val", val);
        object.put("bottom_pool", tableStatus.getTotalPoolMoney());
        object.put("left_chips", 0);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.ALLIN, userIds, id, tableStatus.getRoomId());
    }

    private void checkWaitStatus(String tableId, String roomId) {
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Map<String, String> joinTableUserMap = tableAllUser.getJoinTableUserMap();
        for (String userId : joinTableUserMap.values()) {
             TableUser tableUser = TableUser.load(userId, tableId);
             tableUser.setIsNewJoin("0");
             UserChips userChips = UserChips.load(userId, roomId);
             // 玩家是等待状态而且必须是筹码大于0
             if (tableUser.isWaiting() && userChips != null && userChips.getChips() > 0) {
                 System.out.println("update wait user:" + JSON.toJSONString(tableUser));
                 tableUser.setStatus(GameConstants.USER_STATUS_GAMING);
                 userChips.resetStatus(GameConstants.USER_STATUS_GAMING);
             }
            tableUser.save();
        }
    }

    private void checkExitAndHung(TableStatus tableStatus, TableAllUser tableAllUser, String tableId, String userId, String sitNum, String bigBlindNum, String smallBlindNum) {
        if (sitNum.equalsIgnoreCase(bigBlindNum) || sitNum.equalsIgnoreCase(smallBlindNum)) {
            return;
        }
        if (tableAllUser.isExitOrHang(sitNum)) {
            TableUser tableUser = TableUser.load(userId, tableId);
            tableUser.setBetStatus(GameEventType.FOLD);
            tableUser.save();
            tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), GameEventType.FOLD);
        }
    }
}
