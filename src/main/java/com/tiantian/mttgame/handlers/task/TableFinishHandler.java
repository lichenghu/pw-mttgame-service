package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.*;
import com.tiantian.mttgame.manager.texas.Poker;
import com.tiantian.mttgame.manager.texas.PokerManager;
import com.tiantian.mttgame.manager.texas.PokerOuts;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 *
 */
public class TableFinishHandler implements EventHandler<TableTaskEvent> {

    static Logger LOG = LoggerFactory.getLogger(TableFinishHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        // 回合ID
        //获取桌子玩家人数
        TableStatus tableStatus = TableStatus.load(tableId);
        if (StringUtils.isBlank(tableStatus.getInningId())) {
            // 玩家在下注的时候站起或退出导致该情况
            LOG.info("TableStatus is NULL" + JSON.toJSONString(tableStatus));
            return;
        }
        if(!GameStatus.D_AND_B.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.PRE_FLOP.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.FLOP.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.TURN.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.RIVER.name().equalsIgnoreCase(tableStatus.getStatus())) {
            // 不是正常流程里面进入finish的
            return;
        }

        Map<String, String> sitBetUserIdMap = tableStatus.getSitBetUserIdMap();
        if (sitBetUserIdMap == null) {
            sitBetUserIdMap = new HashMap<>();
        }

        Map<String, String> allUserCards = tableStatus.getUsersCards();

        Map<String, Long> allWinBetMap = new HashMap<>();

        TableAllUser tableAllUser = TableAllUser.load(tableId);
        // 比较牌的大小
        List<Map<String, String>> poolMaps = tableStatus.getBetPoolMapList();
        List<Map<String, Long>> resultList = new ArrayList<>();
        List<String> foldSitNumList = tableStatus.allFoldSitNumList();
        List<String> deskCards = tableStatus.getDeskCardList();
        if (poolMaps != null && poolMaps.size() > 0) {
            for (Map<String, String> poolMap : poolMaps) {
                if (poolMap.size() == 1) {
                    Map<String, Long> sitAndBetsMap = new HashMap<>();
                    Set<Map.Entry<String,String>> poolEntrySet = poolMap.entrySet();
                    Map.Entry<String,String> poolEntry = (Map.Entry<String,String>)poolEntrySet.toArray()[0];
                    long bet = Long.parseLong(poolEntry.getValue());
                    if (bet <= 0) {
                        continue;
                    }
                    sitAndBetsMap.put(poolEntry.getKey(), bet);
                    resultList.add(sitAndBetsMap);
                } else {
                    // 每个池子的下注总额
                    long bet = perTotalPoolBets(poolMap.values());
                    // 单个池子里面下注的人
                    Set<String> poolUsers = poolMap.keySet();

                    // 过滤掉弃牌的玩家
                    Map<String, String> userCards = eliminateFold(allUserCards, poolUsers, foldSitNumList);

                    // 从庄位起始到最后一位，倒序排列，排列后最后一个分的剩下的
                    List<Map.Entry<String, PokerOuts>> outsList = PokerManager.getWinnerPokerOutsList(userCards, deskCards);

                    List<String> winnerSitNum = sortWinnerList(outsList, tableStatus.getButton());

                    Map<String, Long> sitAndBetsMap = getUserBetsMap(winnerSitNum, bet, Long.parseLong(tableStatus.getSmallBlindMoney()));
                    resultList.add(sitAndBetsMap);
                }
            }
        }

        Map<String, String> notFoldCards = tableStatus.allNotFoldSitNumsAndCards();

        // 10大抽1小 规则
        String rule = null;
        // 玩家结算时获取的筹码
        Map<String, Long> userAllMap = userAllBetMap(resultList);
        // 进行玩家的结算
        balanceBets(userAllMap, sitBetUserIdMap, tableId, allWinBetMap, tableStatus.getRoomId());

        String innerId = tableStatus.getInningId();
        String innerCount = tableStatus.getIndexCount();
        float delaySec = 0.5f;
        if (StringUtils.isNotBlank(tableStatus.getNeedDelay())) {
            if("1".equalsIgnoreCase(tableStatus.getNeedDelay())) {
                delaySec += 3f;
            } else if ("2".equalsIgnoreCase(tableStatus.getNeedDelay())) {
                delaySec += 5f;
            }
            else if ("3".equalsIgnoreCase(tableStatus.getNeedDelay())) {
                delaySec += 8f;
            }
        }
        tableStatus.setCurrentBetTimes("");
        tableStatus.setStatus(GameStatus.FINISH.name());
        tableStatus.saveNotAddCnt();

        // 通知玩家信息
        List<Map<String, Object>> userWinList = notifyUserInfo(userAllMap, resultList, tableAllUser.getOnlineTableUserIds(),
                notFoldCards, deskCards, innerId, innerCount, tableStatus.getUsersCards(),
                tableAllUser.userGamingExitHangSitUserId(), false, delaySec, tableId, tableStatus.getRoomId());

        if (userWinList.size() > 0) {
            delaySec += userWinList.size() * 1.5f + 2f;
        }

        long delayMills =  Math.max((long)(delaySec * 1000), 4000) - GameConstants.AFTER_CHECK_CHIPS_DELAYER_TIME;

        // 发送一个延迟3s的开始, 确定庄家大小盲注任务
        jsonObject.put(GameConstants.TASK_EVENT, GameConstants.CHECK_CHIPS);
        jsonObject.put("tableId", tableId);
        jsonObject.put("innerId", innerId);
        jsonObject.put("innerCount", innerCount);
        jsonObject.put("gameId", tableStatus.getRoomId());
        GameUtils.pushTask(context, new TableTaskEvent(GameConstants.CHECK_CHIPS, jsonObject), delayMills);
    }

    private  Map<String, String> eliminateFold(Map<String ,String> allUserCards, Set<String> poolUserSet, List<String> foldList) {
        Map<String, String> poolUserCards = new HashMap<>();
        for (String userSit : poolUserSet) {
            if (allUserCards.containsKey(userSit)) {
                poolUserCards.put(userSit, allUserCards.get(userSit));
            }
        }

        // 筛选出没有弃牌的 玩家座位号
        for (String foldSit : foldList) {
             poolUserCards.remove(foldSit);
        }
        return poolUserCards;
    }

    // 每个池子总下注
    private long perTotalPoolBets( Collection<String> values) {
        // 每个池子的下注总饿
        long bet = 0;
        for (String val : values) {
            bet += Long.parseLong(val);
        }
        return bet;
    }

    private  Map<String, Long> getUserBetsMap(List<String> winnerSitNum ,long bet, long smallBlindMoney) {
        int size = winnerSitNum.size();
        Map<String, Long> sitAndBetsMap = new HashMap<>();
        if(size == 1) {
            sitAndBetsMap.put(winnerSitNum.get(0), bet);
        } else if (size > 1) {
            // 小盲注的个数
            long averageSmallBlindCnt = bet / smallBlindMoney;
            // 剩下的筹码
            long remCnt = bet % smallBlindMoney;
            // 每个玩家分的的小盲注个数
            long average = averageSmallBlindCnt / size;
            // 剩余的小盲注
            long cnt = averageSmallBlindCnt % size;
            for(int i = winnerSitNum.size() - 1; i >= 0; i--) {
                String sitNum = winnerSitNum.get(i);
                long money = average * smallBlindMoney;
                long leftSmall = Math.max(0, cnt) * smallBlindMoney;
                sitAndBetsMap.put(sitNum, (money  + leftSmall +  remCnt));
                remCnt = 0;
                cnt --;
            }
        }
        return sitAndBetsMap;
    }

    // 排序
    private List<String> sortWinnerList(List<Map.Entry<String, PokerOuts>> outsList, String buttonSitNum) {

        List<String> winnerSits = new ArrayList<>();
        for (Map.Entry<String, PokerOuts> entry : outsList) {
            winnerSits.add(entry.getKey());
        }
        // 进行排序
        boolean hasBtnSit = winnerSits.contains(buttonSitNum);
        if (!hasBtnSit) {
            winnerSits.add(buttonSitNum);
        }
        Collections.sort(winnerSits);
        SitCycQueue sitCycQueue = new SitCycQueue(winnerSits.size());
        for (String str : winnerSits) {
            sitCycQueue.addRear(Integer.parseInt(str), buttonSitNum.equals(str));
        }
        int[] returns = sitCycQueue.returnAllByButton();
        List<String> result = new ArrayList<>();
        for (int index : returns) {
            //
            if (!hasBtnSit && index == Integer.parseInt(buttonSitNum)) {
                continue;
            }
            result.add(index + "");

        }
        return result;
    }

    private Map<String, Long> userAllBetMap(List<Map<String, Long>> userBetList) {
        Map<String, Long> betMap = new HashMap<>();
        for (Map<String, Long> userBet : userBetList) {
            Set<Map.Entry<String, Long>> entrySet = userBet.entrySet();
            for (Map.Entry<String, Long> entry : entrySet) {
                String key = entry.getKey();
                Long value = entry.getValue();
                Long oldVal = betMap.get(key);
                if (oldVal != null) {
                    value += oldVal;
                }
                betMap.put(key, value);
            }
        }
        return betMap;
    }

    // 结算
    private void balanceBets(Map<String, Long> winBet, Map<String, String> tableSitUserIdMap, String tableId,
                             Map<String, Long> allWinBetMap, String gameId) {
        Set<Map.Entry<String, Long>> winBetEntrySet = winBet.entrySet();
        for (Map.Entry<String, Long> entry : winBetEntrySet) {
            String key = entry.getKey();
            Long value = entry.getValue();
            String userId = tableSitUserIdMap.get(key);
            if(userId != null) {
                if (allWinBetMap.containsKey(userId)) {
                    Long userWinBet = allWinBetMap.get(userId);
                    if (userWinBet == null) {
                        userWinBet = 0L;
                    }
                    allWinBetMap.put(userId, userWinBet.intValue() + value);
                }
                UserChips userChips = UserChips.load(userId,  gameId);
                if (userChips != null) {
                    userChips.addAndFlushChips(value);
                }
            }
        }
    }

    private List<Map<String, Object>> notifyUserInfo(Map<String, Long> userAllMap, List<Map<String, Long>> resultList,
                                                     Collection<String> onlineTableUserIds, Map<String, String> userCardsMap,
                                                     List<String> deskCards,String innerId, String innerCount,
                                                     Map<String, String> allUserCardsMap, Map<String, String> gamingSitUserMap,
                                                     boolean gameOver, float delaySec, String tableId, String gameId) {
        List<Map<String, Object>> userWinMap = new ArrayList<>();
        Set<Map.Entry<String, Long>> mapSet = userAllMap.entrySet();
        for (Map.Entry<String, Long> entry : mapSet) {
            String sitNum = entry.getKey();
            Long bet = entry.getValue();
            Map<String, Object> map = new HashMap<>();
            if(gamingSitUserMap.containsKey(sitNum)) { // 还在桌子上的玩家
                map.put("sn", Integer.parseInt(sitNum));
                map.put("win", bet);
                userWinMap.add(map);
            }
        }


        List<Object> poolWinInfList = new ArrayList<>();
        // 池序号
        int poolNum = 0;
        for (Map<String, Long> poolMap : resultList) {
            Map<String, Object> poolWinMap = new HashMap<>();
            poolNum ++;
            Set<Map.Entry<String, Long>> poolMapSet = poolMap.entrySet();
            List<Map<String, Object>> perPoolList = new ArrayList<>();
            for (Map.Entry<String, Long> entry : poolMapSet) {
                String sitNum = entry.getKey();
                if(gamingSitUserMap.containsKey(sitNum)) { // 还在桌子上的玩家
                    Long bet = entry.getValue();
                    Map<String, Object> map = new HashMap<>();
                    map.put("sn", Integer.parseInt(sitNum));
                    map.put("win", bet);
                    perPoolList.add(map);
                }
            }
            poolWinMap.put("pool_num", poolNum);
            poolWinMap.put("pool_inf", perPoolList);
            poolWinInfList.add(poolWinMap);
        }
        Map<String, PokerOuts> pokerOutsMap = PokerManager.getUsersPokerOutsList(userCardsMap, deskCards);
        Set<Map.Entry<String, PokerOuts>> pokerSet = pokerOutsMap.entrySet();
        List<Map.Entry<String, PokerOuts>> pokerOutList = new ArrayList<>(pokerSet);
        Collections.sort(pokerOutList, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
        List<Map<String, Object>> userCardsResult = new ArrayList<>();
        int size = pokerOutList.size();

        // 比牌的玩家人数大于1必须亮牌
        if (size > 1) {
            PokerOuts maxPokerOuts = null;
            for (Map.Entry<String, PokerOuts> entry : pokerOutList) {
                String sitNum = entry.getKey();
                // 移除掉已经亮牌的玩家，最后判断需要亮牌的玩家
                allUserCardsMap.remove(sitNum);
                PokerOuts pokerOuts = entry.getValue();
                long level = pokerOuts.getLevel();
                List<Poker> pokerList = pokerOuts.getOutList();
                String pokers = "";
                for (Poker poker : pokerList) {
                    pokers += (poker.getShortPoker() + ",");
                }
                String newPokers = pokers.substring(0, pokers.length() - 1);
                Map<String, Object> oneUserCards = new HashMap<>();
                oneUserCards.put("sn", Integer.parseInt(sitNum));
                oneUserCards.put("level", level);
                oneUserCards.put("hand_cards", userCardsMap.get(sitNum));
                oneUserCards.put("cards", newPokers);
                if (maxPokerOuts == null) {
                    oneUserCards.put("is_max", "1");
                    maxPokerOuts = pokerOuts;
                } else if (pokerOuts.compareTo(maxPokerOuts) >= 0) {
                    oneUserCards.put("is_max", "1");
                    maxPokerOuts = pokerOuts;
                } else {
                    oneUserCards.put("is_max", "0");
                }
                userCardsResult.add(oneUserCards);

            }
        }
//        addShowCards(allUserCardsMap, userCardsResult, gamingSitUserMap, deskCards);

        List<Map<String, Object>> userWinMapList = sortWinOrder(userWinMap, userCardsResult);

        if (userWinMapList.size() > 0) {
            delaySec += userWinMapList.size() * 1.5 + 2f;
        }
        long delayMills =  Math.max((long)(delaySec * 1000), GameConstants.DANDB_DELAYER_TIME) - GameConstants.AFTER_CHECK_CHIPS_DELAYER_TIME;
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCount);
        object.put("pool_win_inf", poolWinInfList); // 池子里面赢得的
        object.put("user_win_inf", userWinMapList); // 玩家总共赢得的
        object.put("user_cards_inf", userCardsResult); //玩家的牌
        object.put("game_over", gameOver ? 1 : 0);
        object.put("delay_ses", delayMills/1000);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.FINISHED, onlineTableUserIds, id, gameId);
        return userWinMapList;
    }

    public static List<Map<String, Object>> sortWinOrder(List<Map<String, Object>> userWinMapList,
                                                         List<Map<String, Object>> userCardsResult) {
        List<Map<String, Object>> newMapList = new ArrayList<>();
        for (Map<String, Object> userCardsMap : userCardsResult) {
            Integer sn = (Integer) userCardsMap.get("sn");
            for (int i = userWinMapList.size() - 1; i >= 0; i--) {
                Map<String, Object> userWinMap = userWinMapList.get(i);
                Integer sn2 = (Integer) userWinMap.get("sn");
                if (sn.intValue() == sn2.intValue()) {
                    newMapList.add(userWinMap);
                    userWinMapList.remove(i);
                }
            }
        }
        if (userWinMapList.size() > 0) {
            newMapList.addAll(userWinMapList);
        }
        return newMapList;
    }

    private void updateRanking(TableStatus tableStatus, TableAllUser tableAllUser) {
        Collection<String> userIds = tableAllUser.getGamingAndExitUserMap().values();
        Map<String, String> userIdSits = Maps.newHashMap();
        for (Map.Entry<String, String> entry : tableAllUser.getGamingAndExitUserMap().entrySet()) {
             String sitNum = entry.getKey();
             String userId = entry.getValue();
             userIdSits.put(userId, sitNum);
        }
        List<UserChips> userChipsList = Lists.newArrayList();
        List<String> overList = Lists.newArrayList();
        for(String userId : userIds) {
            UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
            if (userChips == null || userChips.getChips() == 0) {
                overList.add(userId);
            }
            userChipsList.add(userChips);
        }
        Collections.sort(userChipsList, (o1, o2) -> (int)(o2.getChips() - o1.getChips()));
        int tmpRanking = 1;
        Map<String, Integer> userRanking = Maps.newHashMap();
        long tmpMaxChips = userChipsList.get(0).getChips();
        for (UserChips userChips : userChipsList) {
            long userChip = userChips.getChips();
            if (tmpMaxChips  > userChip) {
                tmpRanking ++;
            }
            else {
                tmpMaxChips = userChip;
            }
            userRanking.put(userChips.getUserId(), tmpRanking);
        }
        List<AllinInf> allinInfs = tableStatus.userAllInfos();
        //overList
        if (overList.size() > 0) {
            List<Integer> overRankings = Lists.newArrayList();
            for (String uId : overList) {
                 Integer uRanking = userRanking.get(uId);
                 if (uRanking != null) {
                     overRankings.add(uRanking);
                 }
            }
            Collections.sort(overRankings); // 升序排列
            int index = 0;
            Collections.sort(allinInfs, (o1, o2) -> {
                int a = (int)(o2.getAllChips() - o1.getAllChips());
                if (a != 0) {
                    return a;
                }
                return o1.getIndex() - o2.getIndex();
            });
//            System.out.println("userRanking:" + JSON.toJSONString(userRanking));
//            System.out.println("overList:" + JSON.toJSONString(overList));
//            System.out.println("overRankings:" + JSON.toJSONString(overRankings));
//            System.out.println("allinInfs:" + JSON.toJSONString(allinInfs));
            for (AllinInf allinInf : allinInfs) {
                String allInUserId = allinInf.getUserId();
                if (!overList.contains(allInUserId)) { // 必须是结束的
                    continue;
                }
                if (index >= overRankings.size()) {
                    continue;
                }
                userRanking.put(allInUserId, overRankings.get(index));
                index++;
            }
        }

        String roomId = tableStatus.getRoomId();
        String gameInfo = tableStatus.getGameInfo();
        GameStartEvent.Game game = JSON.parseObject(gameInfo, GameStartEvent.Game.class);
        List<GameStartEvent.Reward> rewards = game.getRewards();
        Map<String, Map<String, String>> userRewardMap = Maps.newHashMap();
        Map<String, Long> userScoreMap = Maps.newHashMap();
        // 排名的玩家
        Map<Integer, List<String>> rankingUserMap = Maps.newHashMap();
        for (Map.Entry<String, Integer> entry : userRanking.entrySet()) {
            String userId = entry.getKey();
            Integer ranking = entry.getValue();
            List<String> uIds = rankingUserMap.get(ranking);
            if (uIds == null) {
                uIds = Lists.newArrayList();
            }
            uIds.add(userId);
            rankingUserMap.put(ranking, uIds);
        }

        System.out.println("rewards" + JSON.toJSONString(rewards));
        if (rewards != null) {
            for (GameStartEvent.Reward reward : rewards) {
                Integer ranking = reward.getRanking();
                Long score = reward.getVirtualNums();
                List<String> uIds = rankingUserMap.get(ranking);
                if (uIds == null) {
                    continue;
                }
                if (score > 0) {
                    long perScore = score / uIds.size();
                    long leftScore = score % uIds.size();
                    for (String uId : uIds) {
                        leftScore--;
                        long userScore = perScore;
                        if (leftScore >= 0) {
                            userScore++;
                        }
                        userScoreMap.put(uId, userScore);
                    }
                }
                if (StringUtils.isNotBlank(reward.getPhysicalId())) {
                    Map<String, String> rewardMap = Maps.newHashMap();
                    rewardMap.put("rewardId", reward.getPhysicalId());
                    rewardMap.put("rewardName", reward.getPhysicalName());
                    userRewardMap.put(uIds.get(0), rewardMap);
                }
            }
        }
        System.out.println("userRewardMap" + JSON.toJSONString(userRewardMap));
        for (String gamingUserId : userIds) {
            // 通知玩家离开
            Integer ranking = userRanking.get(gamingUserId);
            JSONObject object1 = new JSONObject();
            object1.put("inner_id", tableStatus.getInningId());
            object1.put("inner_cnt", tableStatus.getIndexCount());
            object1.put("uid", gamingUserId);
            object1.put("ranking", ranking == null ? "1": ranking.toString());
            object1.put("is_over", "1");
            object1.put("sn", Integer.parseInt(userIdSits.get(gamingUserId)));
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUser(object1, GameEventType.RANKING, gamingUserId, tableStatus.getRoomId());
        }

        System.out.println("userScoreMap" + JSON.toJSONString(userScoreMap));
        System.out.println("userRanking" + JSON.toJSONString(userRanking));
        // 修改所有人的排名
//        ClusterActorManager.tellRoom(new UserRankingEvent(roomId, tableStatus.getRoomName(), tableStatus.getTableId(), userRanking,
//                userScoreMap, userRewardMap, new ArrayList<>(userIds)));
    }
}
