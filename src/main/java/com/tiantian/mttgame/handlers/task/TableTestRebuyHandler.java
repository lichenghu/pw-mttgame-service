package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.LocalTableRoundEndEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class TableTestRebuyHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableTestRebuyHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            LOG.error("test rebuy error tableStatus is null");
            return;
        }
        //  牌局已经不是当前牌局
        if (StringUtils.isBlank(tableStatus.getInningId()) || !inningId.equals(tableStatus.getInningId())) {
            LOG.error("test rebuy error InningId is error, tableStatus inningId :"+ tableStatus.getInningId() +"," +
                    "inningId:" + inningId);
            return;
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.userSitDownUsers().values();
        for (String userId : userIds) {
            TableUser tableUser = TableUser.load(userId, tableId);
            if (tableUser == null || tableUser.isNull()) {
                continue;
            }
            String userTableId = tableUser.getTableId();
            if (tableId.equalsIgnoreCase(userTableId)) {
                tableUser.setOperateCount(null);
                tableUser.setBetStatus("");
                tableUser.setShowCards("");
            }
            // 判断筹码
            checkUserChips(userId, tableUser.getSitNum(), tableAllUser.getOnlineTableUserIds(), tableUser,
                    tableStatus.getInningId(), tableStatus.getIndexCount(), tableAllUser, tableStatus.getRoomId());
        }
        List<String> overList = tableStatus.overUserList();
        Map<String, Long> userLeftChips = tableStatus.userLeftChipsMap();
        tableStatus.clearNotFlush();
        tableStatus.save();

        context.parent().tell(new LocalTableRoundEndEvent(tableId, event.gameId(), overList, userLeftChips),
                ActorRef.noSender());
    }


    private void checkUserChips(String userId,
                                String sitNum, Collection<String> toUserIds, TableUser tableUser,
                                String innerId, String innerCnt, TableAllUser tableAllUser,
                                String gameId) {
        UserChips userChips = UserChips.load(userId, gameId);
        if (userChips != null && userChips.getChips() == 0) {
            userChips.delSelf(gameId); //删除筹码
            if (tableUser == null || tableUser.isNull()
                    || "standing".equalsIgnoreCase(tableUser.getStatus())) {
                return;
            }
            // 站起
            tableUser.delSelf(tableUser.getTableId());
            // tableAllUser.userStandUp(userId);
            tableAllUser.deleteUserByUserId(userId);
            // 通知玩家离开
            JSONObject object1 = new JSONObject();
            object1.put("inner_id", innerId);
            object1.put("inner_cnt", innerCnt);
            object1.put("uid", userId);
            object1.put("sn", Integer.parseInt(sitNum));
            object1.put("reason", "");
            String id = UUID.randomUUID().toString().replace("-", "");
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUsers(object1, GameEventType.STAND_UP, toUserIds, id, gameId);
        } else {
            tableUser.save();
        }
    }

}
