package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.utils.GameUtils;

/**
 *
 */
public class TableTestStatusHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        String nextStatus = jsonObject.getString("selector");
        // 触发下一轮的发牌事件
        GameUtils.nextStatusTask(GameStatus.valueOf(nextStatus), tableId, inningId, self, context, sender);
    }
}
