package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.user.GameUserAllinEvent;
import com.tiantian.mtt.akka.event.user.GameUserCallEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.Handlers;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class TableTestBetHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableTestBetHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        String sitNum = jsonObject.getString("sitNum");
        String userId = jsonObject.getString("userId");
        String operateCount = jsonObject.getString("operateCount");
        String pwdParam = jsonObject.getString("pwd");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        if (!tableStatus.checkPwd(pwdParam)) {
            return;
        }
        //  牌局已经不是当前牌局
        if (!inningId.equals(tableStatus.getInningId())) {
            return;
        }
        //  牌局已经结束
        if (GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }

        String currentBet = tableStatus.getCurrentBet();
        // 已经当前需要下注的不是任务中的人
        if (currentBet == null || !currentBet.equals(sitNum)) {
            return;
        }
        // 判断是否弃牌
        Map<String, String> allNotFolds = tableStatus.allNotFoldSitNumsAndCards();
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        String status = null;
        // 没有弃牌
        if (allNotFolds != null && allNotFolds.containsKey(sitNum)) {
            String tableUserId = tableAllUser.userSitDownUsers().get(sitNum);
            // 位置上的人不是当前桌子座位玩家，可能任务的玩家已经退出了
            if (tableUserId == null || !tableUserId.equals(userId)) {
                LOG.error("tableUserId is null or tableUserId no eq userId, tableUserId is :" + tableUserId);
                return;
            }
            TableUser tableUser = TableUser.load(userId, tableId);
            String userTableId = tableUser.getTableId();
            String userSitNum = tableUser.getSitNum();
            String userOperateCount = tableUser.getOperateCount();
            if (StringUtils.isBlank(userOperateCount)) {
                tableUser.setOperateCount("1");
            }
            if (!tableId.equals(userTableId)) {
                return;
            }
            if (!sitNum.equals(userSitNum)) {
                return;
            }
            // 玩家当前操作序号大于检测时候的版本号说明已经玩家操作过一次，该次检测跳过
            if (userOperateCount != null && (Integer.parseInt(userOperateCount) > Integer.parseInt(operateCount))) {
                return;
            }

            UserChips currUserChips = UserChips.load(userId, tableStatus.getRoomId());
            String[] currOps = tableStatus.getUserCanOps(userId, userSitNum);

            char op = currOps[0].charAt(1);
            // 可以让牌
            if ('1' == op) {
                status = GameEventType.CHECK;
            } else {
                status = GameEventType.FOLD;
            }
            //TODO 测试用的代码
            /** **/
            char canAllin2 = currOps[0].charAt(4);
            if ('1' == canAllin2 && (tableUser.getNickName() != null &&
                                tableUser.getNickName().contains("Mobile"))) {
                if (Math.random() > 0.6) { //  allin

                    // allin
                    GameUserAllinEvent gameUserAllinEvent = new GameUserAllinEvent(userId, pwdParam, tableStatus.getRoomId());
                    gameUserAllinEvent.setTbId(tableId);
                    Handlers.INSTANCE.execute(gameUserAllinEvent, self, context, sender);
                    return;
                }
            }
            /** **/

            if (tableUser.isExit() || tableUser.isHang() || (tableUser.getTotalSecs() != null &&
                                Integer.parseInt(tableUser.getTotalSecs()) < GameConstants.BET_DELAYER_TIME / 1000)) { // 玩家已经退出房间,或者挂机
                UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
                // 当玩家筹码小于等于一个大盲注的时候自动跟注或者allin
                if (userChips != null && userChips.getChips() <= Long.parseLong(tableStatus.getBigBlindMoney())) {
                    char canCall = currOps[0].charAt(2);
                    if ('1' == canCall) {  // 判断能不能跟注
                        //call
                        GameUserCallEvent gameUserCallEvent = new GameUserCallEvent(userId, pwdParam,  tableStatus.getRoomId());
                        gameUserCallEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserCallEvent, self, context, sender);
                        return;
                    }
                    char canAllin = currOps[0].charAt(4);
                    if ('1' == canAllin) {
                        // allin
                        GameUserAllinEvent gameUserAllinEvent = new GameUserAllinEvent(userId, pwdParam, tableStatus.getRoomId());
                        gameUserAllinEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserAllinEvent, self, context, sender);
                        return;
                    }
                }
            }

            //TODO mtt中不合理 玩家不操作的时候, 强制allin
      //      if (smallMoney >= buyIn / 2) {
//                GameUserAllinEvent gameUserAllinEvent = new GameUserAllinEvent(userId, pwdParam, tableStatus.getRoomId());
//                gameUserAllinEvent.setTbId(tableId);
//                Handlers.INSTANCE.execute(gameUserAllinEvent, self, context, sender);
//                // 增加玩家的强制操作
//                String oldNotOpOperateCount = tableUser.getNotOperateCount();
//                if (StringUtils.isBlank(oldNotOpOperateCount)) {
//                    oldNotOpOperateCount = "0";
//                }
//                // 增加玩家的操作计数
//                tableUser.setNotOperateCount((Integer.parseInt(oldNotOpOperateCount) + 1) + "");
//                // 玩家几次没有操作
//                if (Integer.parseInt(tableUser.getNotOperateCount()) >= GameConstants.MAX_NOT_OPERATE_COUNT) {
//                    //减少CD
//                    long userTotalSecs = Long.parseLong(tableUser.getTotalSecs());
//                    if (userTotalSecs > 3) { // 2次没操作直接修改成3s的下注时间
//                        userTotalSecs = 3;
//                    }
//                    tableUser.setTotalSecs(tableUser.isExit() ? 1 + "" : userTotalSecs + "");
//                }
//                // 刷新setProperty 数据
//                tableUser.save();
//                return;
//            }

            // 增加玩家的操作计数
            tableUser.addOneOperateCount();
            // 增加玩家的强制操作（当强制玩家2次后，玩家自动站起）
            String oldNotOpOperateCount = tableUser.getNotOperateCount();

            tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);

            if (StringUtils.isBlank(oldNotOpOperateCount)) {
                oldNotOpOperateCount = "0";
            }
            // 增加玩家的操作计数
            tableUser.setNotOperateCount((Integer.parseInt(oldNotOpOperateCount) + 1) + "");
            tableUser.setBetStatus(status);
            //玩家几次没有操作
            if ( Integer.parseInt(tableUser.getNotOperateCount()) >= GameConstants.MAX_NOT_OPERATE_COUNT) {
                if (!(tableUser.getNickName() != null &&
                        tableUser.getNickName().contains("Mobile"))) { // TODO 测试代码

//                //减少CD
//                long userTotalSecs = Long.parseLong(tableUser.getTotalSecs());
//                if (userTotalSecs > 3) { // 2次没操作直接修改成5s的下注时间
//                    userTotalSecs = 3;
//                }
//                tableUser.setTotalSecs(tableUser.isExit() ? 1 + "" : userTotalSecs + "");
                tableUser.setStatus(GameConstants.USER_STATUS_HANG);
                UserChips userChips = UserChips.load(userId, event.gameId());
                if (userChips != null) {
                    userChips.resetStatus(GameConstants.USER_STATUS_HANG);
                }
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("user_id", userId);
                object.put("sit_num", tableUser.getSitNum());
                GameUtils.notifyUsers(object, GameEventType.MTT_HANG, userIds,
                        UUID.randomUUID().toString().replace("-", ""), tableStatus.getRoomId());
                }
            }

            // 通知其他人 该玩家的下注状态  看牌, 弃牌
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(sitNum));
            object.put("left_chips", currUserChips == null ? 0 : currUserChips.getChips());
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, status, userIds, id, tableStatus.getRoomId());

            // 刷新setProperty 数据
            tableUser.save();
        }

        //测试游戏状态: 是否进行下流程: flop turn river fish
        Set<String> canBetSits = tableStatus.canBetSits();
        String maxBetSitNum = tableStatus.getMaxBetSitNum();

        String currentStatus = tableStatus.getStatus();

        GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, currentStatus, maxBetSitNum, sitNum);
        if (nextStatus != null) {
            //完成一轮的下注结算，分池
            tableStatus.roundBetEnd();
            if (status != null && (status.equalsIgnoreCase(GameEventType.FOLD) ||
                    status.equalsIgnoreCase(GameEventType.ALLIN))) {
                boolean needShow = tableStatus.needShowAllCards();
                if (needShow) { // 需要显示所有的手牌
                    List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                    JSONObject object2 = new JSONObject();
                    object2.put("inner_id", tableStatus.getInningId());
                    object2.put("inner_cnt", tableStatus.getIndexCount());
                    object2.put("all_cards", cardsList);
                    String id2 = UUID.randomUUID().toString().replace("-", "");
                    GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableStatus.getRoomId());
                }
            }
            // 保存数据
            tableStatus.save();
            // 通知玩家的池信息
            GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
            // 触发下一轮的发牌事件
            GameUtils.triggerNextStatusTask(nextStatus, tableId, inningId, context, tableStatus.getRoomId());
            return;
        }

        // 触发下一个玩家的30s定时检测下注任务
        // 获取下个下注的玩家座位号
        String nextBetSitNum = GameUtils.getNextBetSitNum(canBetSits, sitNum, maxBetSitNum);
        String nextBetUserId = tableAllUser.userSitDownUsers().get(nextBetSitNum);
        TableUser nextTableUser = TableUser.load(nextBetUserId, tableId);

        String nextUserOperateCount = nextTableUser.getOperateCount();
        if (StringUtils.isBlank(nextUserOperateCount)) {
            nextUserOperateCount = "1";
            nextTableUser.setOperateCount(nextUserOperateCount);
            nextTableUser.save();
        }
        // 设置当前需要下注的人的座位号
        tableStatus.setCurrentBet(nextBetSitNum);
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");
        String pwd = tableStatus.randomPwd();
        tableStatus.save();
        if(tableAllUser.isOnline(nextBetUserId)) { // 玩家没有退出挂机
            // 通知玩家下注
            JSONObject nextObject = new JSONObject();
            nextObject.put("inner_id", tableStatus.getInningId());
            nextObject.put("inner_cnt", tableStatus.getIndexCount());
            nextObject.put("uid", nextBetUserId);
            nextObject.put("sn", Integer.parseInt(nextBetSitNum));
            nextObject.put("t", Long.parseLong(nextTableUser.getTotalSecs()));
            String[] ops = tableStatus.getUserCanOps(nextBetUserId, nextBetSitNum);
            nextObject.put("c_b", StringUtils.join(ops, ",")); // 玩家可以操作的选项
            nextObject.put("pwd", pwd);
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUser(nextObject, GameEventType.BET, nextBetUserId, tableStatus.getRoomId());
        }
        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", tableStatus.getIndexCount());
        otherNextObject.put("uid", nextBetUserId);
        otherNextObject.put("sn", Integer.parseInt(nextBetSitNum));
        otherNextObject.put("t", Long.parseLong(nextTableUser.getTotalSecs()));
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", pwd);
        userIds.remove(nextBetUserId);
        // 如果不捕获异常则会终端下一个任务
        String newId = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableStatus.getRoomId());
        int newOperateCount = Integer.parseInt(nextUserOperateCount);
        // 触发30s的检测任务 大盲注下一位的状态 如果已经操作则跳过, preFlop
        GameUtils.triggerBetTestTask(context, tableId, inningId, nextBetSitNum, nextBetUserId, newOperateCount, pwd, nextTableUser.isExit(),
                Long.parseLong(nextTableUser.getTotalSecs()), tableStatus.getRoomId());
    }
}
