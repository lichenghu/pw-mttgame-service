package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.data.redis.RedisUtil;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.Handlers;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 *
 */
public class TableBeginHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableBeginHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String randomBtn = jsonObject.getString("randomBtn");
        String users = jsonObject.getString("game_users");
        Integer userCnt = jsonObject.getInteger("game_users_cnt");
        String userIds = jsonObject.getString("game_uids");
        LOG.info("userCnt:" + userCnt);

        LOG.info("game_users: " + users + ", tableId:" + tableId);

        if (randomBtn == null) {
            randomBtn = "";
        }
        // 从redis中加载桌子状态数据
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null) {
            LOG.error("TableStatus is Null");
            return;
        }
        // 如果不是准备则表示游戏已经在开始状态
        if (!GameStatus.READY.name().equalsIgnoreCase(tableStatus.getStatus())
                && !GameStatus.INIT.name().equalsIgnoreCase(tableStatus.getStatus())) {
            LOG.error("GameStatus is not READY or INIT, GameStatus is :" + tableStatus.getStatus()
                            + ", tableId: + " + tableStatus.getTableId());
            return;
        }
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser.getJoinTableUserMap() == null) {
            LOG.error("JoinTableUsers is Null");
            return;
        }
        if (userCnt != null && userCnt != tableAllUser.userSitDownUsers().size()) {
            LOG.error("game_users is not equal:userCnt:" + userCnt +", JoinTableUserMap" +
                    tableAllUser.userSitDownUsers().size() +
                    ", joinUsers:" + JSON.toJSONString(tableAllUser.userSitDownUsers().values()));
            if (userIds != null) {
                String[] ids = userIds.split(",");
                for(String id : ids) {
                    if (StringUtils.isNotBlank(id)) {
                        if (!tableAllUser.userSitDownUsers().values().contains(id)) {
                             TableUser tableUser = TableUser.load(id, tableId);
                             LOG.info("TableUser:" + JSON.toJSONString(tableUser));
                        }
                    }
                }
            }
        }
        // 第一次begin,判断人数是否齐了
        if (StringUtils.isNotBlank(randomBtn)
                && tableAllUser.getJoinTableUserMap().size() < Integer.parseInt(tableStatus.getMinUsers())) { //判断人数
            LOG.error("RandomBtn is Null or JoinTableUses is little than MinUsers, RandomBtn is: " + randomBtn +
                        "JoinTableUses is :" + tableAllUser.getJoinTableUserMap().size() +
                    ",MinUsers is :" + tableStatus.getMinUsers());
            return;
        }
        if(!tableStatus.isStarted()) { // 开始游戏标志
            tableStatus.gameStart();
        }
        Set<Map.Entry<String, String>> entrySet = tableAllUser.getJoinTableUserMap().entrySet();
        Iterator<Map.Entry<String, String>> iterator = entrySet.iterator();
        List<String> removeSitList = new ArrayList<>();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String userId = entry.getValue();
            String userRoomTableKey = GameConstants.USER_SPINGO_TABLE_KEY + userId + ":" +tableId;
            String mapTableId = RedisUtil.getFromMap(userRoomTableKey, "tableId");
            if (mapTableId == null || !mapTableId.equalsIgnoreCase(tableId)) {
                removeSitList.add(entry.getKey());
                // 需要删除
                iterator.remove();
            }
        }

        if (removeSitList.size() > 0) {
            tableAllUser.flushGamingSitUser(removeSitList);
        }

        tableStatus.setStatus(GameStatus.BEGIN.name());
        tableStatus.setRestMills("");
        tableStatus.setStartRestTimeMills("");
        // 初始化大小盲注
        if (StringUtils.isBlank(tableStatus.getSmallBlindMoney())) {
            GameStartEvent.Game game = tableStatus.game() ;
            GameStartEvent.Rule gameRules = game.getRules().get(0);
            tableStatus.setSmallBlindMoney(gameRules.getSmallBlind() + "");
            tableStatus.setBigBlindMoney(gameRules.getBigBlind() + "");
            tableStatus.setCurrentBlindLevel(1 + "");
            tableStatus.setAnte(gameRules.getAnte() + "");
            tableStatus.setBuyIn(game.getStartBuyIn() + "");
        }

        tableStatus.saveNotAddCnt();

        JSONObject data = new JSONObject();
        data.put(GameConstants.TASK_EVENT, GameStatus.D_AND_B.name());
        data.put("tableId", tableId);
        data.put("randomBtn", randomBtn);
        data.put("gameId", tableStatus.getRoomId());
        TableTaskEvent tableTaskEvent = new TableTaskEvent(GameStatus.D_AND_B.name(), data);
        Handlers.INSTANCE.execute(tableTaskEvent, self, context, sender);
    }
}
