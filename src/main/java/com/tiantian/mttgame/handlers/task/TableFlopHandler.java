package com.tiantian.mttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.texas.Poker;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class TableFlopHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        // 回合ID
        String inningId = jsonObject.getString("inningId");
        //从redis中获取三张牌（先烧一张） 发给玩家 然后再把剩下的牌保存到redis
        //三张牌
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null || StringUtils.isBlank(tableStatus.getCards())) {
            return;
        }
        if (!GameStatus.PRE_FLOP.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }
        // 已经发过牌了
        if (tableStatus.getDeskCardList() != null && tableStatus.getDeskCardList().size() > 0) {
            return;
        }
        List<Poker> pokerList = JSON.parseArray(tableStatus.getCards(), Poker.class);
        pokerList.remove(0);
        List<String> threeCards = new ArrayList<>();
        Poker poker1 = pokerList.remove(0);
        threeCards.add(poker1.getShortPoker());
        Poker poker2 = pokerList.remove(0);
        threeCards.add(poker2.getShortPoker());
        Poker poker3 = pokerList.remove(0);
        threeCards.add(poker3.getShortPoker());

        tableStatus.setCards(JSON.toJSONString(pokerList));
        // 设置当局的牌状态为flop
        tableStatus.setStatus(GameStatus.FLOP.name());
        tableStatus.setDeskCards(JSON.toJSONString(threeCards));
        tableStatus.checkDelay();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        //发送三张牌的信息给玩家

        JSONObject object = new JSONObject();
        String flopCards = StringUtils.join(threeCards, ",");
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("flop_cards", flopCards);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.FLOP_CARDS, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());
        GameUtils.noticeCardsLevel(tableStatus, tableAllUser);
        // 校验是否直接进行下一个发牌
        GameStatus nextStatus = GameUtils.beforeCheckNextStatus(GameStatus.FLOP.name(), tableStatus);
        if (nextStatus != null) {
            tableStatus.roundBetEnd();
            tableStatus.save();
            // 通知玩家的池信息
            GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
            // 触发下一轮的发牌事件
            GameUtils.nextStatusTask(nextStatus, tableId, inningId,
                    self, context, sender);
            return;
        }

        //通知下一个玩家下注
        GameUtils.noticeNextUserBet(tableId, tableStatus, null, true, null,
                self, context, sender);
    }
}
