package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mttgame.akka.event.LocalTableStartEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;
import com.tiantian.mttgame.utils.GameUtils;
import java.util.*;

/**
 *
 */
public class TableStartHandler implements EventHandler<LocalTableStartEvent> {
    @Override
    public void handler(LocalTableStartEvent event, ActorRef self,
                         UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        List<String> userIds = event.getUserIds();
        GameStartEvent.Game game = event.getGame();
        long netxUpBlindTimes = event.getNetxUpBlindTimes();
        boolean canStart = event.isCanStart();
        TableStatus tableStatus = TableStatus.init(tableId);
        tableStatus.setRoomId(game.getGameId());
        tableStatus.setGameInfo(JSON.toJSONString(game));
        tableStatus.setInningId(UUID.randomUUID().toString().replace("-", ""));
        tableStatus.setStatus(GameStatus.INIT.name());
        tableStatus.setMinUsers(6 + "");
        tableStatus.setMaxUsers(game.getTableUsers() + "");
        tableStatus.gameStop(); // 游戏未开始
        tableStatus.setLeftUsers(event.getTotalUsers() + "");
        tableStatus.setAverageChips(event.getAverageChips() + "");
        tableStatus.setMaxCanRebuyBlindLvl(game.getCanRebuyBlindLvl() + "");
        tableStatus.setReBuyInCnt(game.getBuyInCnt() + "");
        tableStatus.setCurrentBlindLevel(1 + ""); //设置盲注级别
        tableStatus.setNextUpBlindTimes(netxUpBlindTimes + "");
        tableStatus.setDelaySignStopMills((game.getStartMill() + game.getSignDelayMins() * 60000) + "");
        tableStatus.saveNotAddCnt();

        long beginChips = game.getStartBuyIn();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        int i = 0;
        for (GameStartEvent.UserInfo userInfo : event.getUserInfos()) {
             String sitNum = (i + 1) + "";
             TableUser tableUser = TableUser.init(userInfo.getUserId(), tableId);
             tableUser.setSitNum(sitNum);
             tableUser.setRoomId(event.getGameId());
             tableUser.setNickName(userInfo.getNickName());
             tableUser.setAvatarUrl(userInfo.getAvatarUrl());
             tableUser.setStatus(GameConstants.USER_STATUS_GAMING);
             tableUser.setGender(userInfo.getGender());
             tableUser.setTotalSecs(GameConstants.BET_DELAYER_TIME / 1000 + "");
             tableUser.setRanking("1");
             tableUser.save();
             tableAllUser.joinUser(userInfo.getUserId(), sitNum, false);
             tableAllUser = TableAllUser.load(tableId); // 重新刷新数据
             UserChips.init(userInfo.getUserId(), event.getGameId(), beginChips);
             i++;
        }
        if (canStart) { // 直接开始
            tableStatus.gameStart(); // 游戏开始
            tableStatus.setStatus(GameStatus.INIT.name());
            tableStatus.setStartMills((System.currentTimeMillis()
                        + GameConstants.INIT_TO_BEGIN_DELAYER_TIME * 1000) + "");
            tableStatus.saveNotAddCnt();
            // 倒计时开始游戏
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("game_id", tableStatus.getRoomId());
            object.put("left_secs", GameConstants.INIT_TO_BEGIN_DELAYER_TIME);
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.TABLE_BEGIN, userIds, id,  null);

            //随机一个庄位
            JSONObject object2 = new JSONObject();
            object2.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
            object2.put("tableId", tableId);
            object2.put("gameId", game.getGameId());
            object2.put("inningId", tableStatus.getInningId());
            Random random = new Random();
            TableAllUser $tableAllUser = TableAllUser.load(tableId); // 需要重新加载
            Set<String> tableSitSets = $tableAllUser.getAllSitNums();
            List<String> sitLists = new ArrayList<>(tableSitSets);
            int randomIndex = random.nextInt(sitLists.size());
            //System.out.println(randomIndex + "==randomIndex");
           // System.out.println(userIds + "==userIds");
            object2.put("randomBtn",  sitLists.get(randomIndex));
            GameUtils.pushTask(context, new TableTaskEvent(GameStatus.BEGIN.name(), object2),
                    GameConstants.INIT_TO_BEGIN_DELAYER_TIME);
        }
        else {
            //TODO 发送等待消息

        }
        sender.tell(true, ActorRef.noSender());
    }
}
