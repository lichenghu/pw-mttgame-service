package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.LocalTableUpdateRankingsEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

/**
 *
 */
public class TableUpdateRankingHandler implements EventHandler<LocalTableUpdateRankingsEvent> {
    private static Logger LOG = LoggerFactory.getLogger(TableUpdateRankingHandler.class);
    @Override
    public void handler(LocalTableUpdateRankingsEvent event, ActorRef self, UntypedActorContext context,
                        ActorRef sender) {
        TableStatus tableStatus = TableStatus.load(event.tableId());
        if (tableStatus == null) {
            LOG.error("TableStatus is null, tableId is " + event.tableId());
            return;
        }
        tableStatus.setAverageChips(event.getAverage() + "");
        tableStatus.setLeftUsers(event.getLeftUsers() + "");
        tableStatus.saveNotAddCnt();
        TableAllUser tableAllUser = TableAllUser.load(event.tableId());
        if (event.getUserRankings() != null) {
            for (Map<String, String> rankingInfo : event.getUserRankings()) {
                 String userId = rankingInfo.get("userId");
                 String ranking = rankingInfo.get("ranking");
                 TableUser tableUser = TableUser.load(userId, event.tableId());
                 if (tableUser != null && !tableUser.isNull()) {
                     tableUser.setRanking(ranking);
                     tableUser.save();
                     if (tableAllUser != null && tableAllUser.isOnline(userId)) {
                         sendUserRankingInfos(tableStatus, userId, event.getLeftUsers() + "", ranking, event.getAverage() + "");
                     }
                 }
            }
        }
    }

    private void sendUserRankingInfos(TableStatus tableStatus, String userId, String leftUsers, String ranking,
                                      String averageChips) {
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId() == null ? "" : tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount() == null ? "" : tableStatus.getIndexCount());
        object.put("left_users", leftUsers);
        object.put("ranking", ranking);
        object.put("average_chips", averageChips);
        GameUtils.notifyUser(object, GameEventType.RANKING_UPDATE, userId, tableStatus.getRoomId());
    }
}
