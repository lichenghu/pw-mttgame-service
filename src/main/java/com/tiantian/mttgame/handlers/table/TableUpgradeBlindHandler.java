package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.LocalTableUpgradeBlindEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.utils.GameUtils;

import java.util.UUID;

/**
 *
 */
public class TableUpgradeBlindHandler implements EventHandler<LocalTableUpgradeBlindEvent> {
    @Override
    public void handler(LocalTableUpgradeBlindEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        long ante = event.getNextAnte();
        long bigBlind = event.getNextBigBlind();
        long smallBlind = event.getNextSmallBlind();
        long upgradeTime = event.getNextUpgradeTime();
        int lvl = event.getNextLvl();
        TableAllUser tableAllUser = TableAllUser.load(event.tableId());
        TableStatus tableStatus = TableStatus.load(event.tableId());
        if (tableStatus == null) {
            return;
        }
        tableStatus.setAnte(ante + "");
        tableStatus.setSmallBlindMoney(smallBlind + "");
        tableStatus.setBigBlindMoney(bigBlind + "");
        tableStatus.setNextUpBlindTimes(upgradeTime + "");
        tableStatus.setCurrentBlindLevel(lvl + "");
        tableStatus.saveNotAddCnt();
        //发送升级盲注信息
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId() == null ? "" : tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount() == null ? "" : tableStatus.getIndexCount());
        object.put("small_blind", event.getNextSmallBlind() + "");
        object.put("big_blind", event.getNextBigBlind() + "");
        object.put("ante", event.getNextAnte() + "");
        object.put("lvl", event.getNextLvl());
        object.put("next_upgrade_secs", event.getUpgradeSecs());
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.BLIND_UP, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());
    }
}
