package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.GameEvent;
import com.tiantian.mttgame.akka.event.LocalTableAddUserEvent;
import com.tiantian.mttgame.akka.event.LocalTableRoundEndEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.swing.StringUIClientPropertyKey;

import java.util.*;

/**
 *
 */
public class TableAddUserHandler implements EventHandler<LocalTableAddUserEvent> {
    private static Logger LOG = LoggerFactory.getLogger(TableAddUserHandler.class);
    @Override
    public void handler(LocalTableAddUserEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        try {
            String tableId = event.getTableId();
            LOG.info("TableAddUserHandler:" + JSON.toJSONString(event));
            List<LocalTableAddUserEvent.UserInfo> userInfoList = event.getUserInfos();
            if (userInfoList == null || userInfoList.size() == 0) {
                LOG.error("userInfoList is empty");
                return;
            }
            TableStatus tableStatus = TableStatus.load(tableId);
            if (tableStatus == null) {
                LOG.error("TableStatus is null");
                return;
            }
            String maxUsers = tableStatus.getMaxUsers();
            TableAllUser tableAllUser = TableAllUser.load(tableId);
            Collection<String> oldUsers = tableAllUser.getOnlineTableUserIds();
            int size = tableAllUser.getJoinTableUserMap().size();
            if (size + userInfoList.size() > Integer.parseInt(maxUsers)) {
                LOG.error("超过最大人数,原来人数:" + size + ",增加人数:" + userInfoList.size()
                        + ",最大人数:" + Integer.parseInt(maxUsers) + ",JoinTableMap:"
                        + JSON.toJSONString(tableAllUser.getJoinTableUserMap()));
                return;
            }
            List<String> sits = new ArrayList<>();
            Set<String> allSitNums = tableAllUser.getAllSitNums();
            for (int i = 1; i <= Integer.parseInt(maxUsers); i++) {
                if (!allSitNums.contains(i + "")) {
                    sits.add(i + "");
                }
            }
            List<String> addUserIds = new ArrayList<>();
            for (LocalTableAddUserEvent.UserInfo userInfo : userInfoList) {
                LOG.info("Table add user: userId :" + userInfo.getUserId() + ", tableId : " + tableId);
                String sitNum = sits.remove(0);
                addUserIds.add(userInfo.getUserId());
                UserChips userChips = UserChips.load(userInfo.getUserId(), tableStatus.getRoomId());
                String status = GameConstants.USER_STATUS_WAIT;
                if (userChips != null) {
                    LOG.info("change table chips status:" + userChips.getStatus() + ",change table chips userId :" + userInfo.getUserId());
                    status = StringUtils.isBlank(userChips.getStatus()) ? status : userChips.getStatus();
                } else {
                    if (event.getInitChips() > 0) { //玩家延时报名进入的
                         userChips = UserChips.init(userInfo.getUserId(), tableStatus.getRoomId(), event.getInitChips());
                         if (GameStatus.INIT.name().equalsIgnoreCase(tableStatus.getStatus())) {
                             status = GameConstants.USER_STATUS_GAMING;
                         }
                         userChips.resetStatus(status);
                    } else {
                        LOG.error("change table chips is null, userId:" + userInfo.getUserId());
                    }
                }
                LOG.info("change table status:" + status);
                TableUser tableUser = TableUser.init(userInfo.getUserId(), tableId);
                tableUser.setSitNum(sitNum);
                tableUser.setRoomId(event.getGameId());
                tableUser.setNickName(userInfo.getNickName());
                tableUser.setAvatarUrl(userInfo.getAvatarUrl());
                tableUser.setGender(userInfo.getGender());
                tableUser.setStatus(status);
                tableUser.setTotalSecs(GameConstants.BET_DELAYER_TIME / 1000 + "");
                tableUser.setRanking(userInfo.getRanking());
                tableUser.setIsNewJoin("1");
                tableUser.save();
                tableAllUser.joinUser(userInfo.getUserId(), sitNum, true);

                long money = 0;
                if (userChips != null) {
                    money = userChips.getChips();
                }
                //通知其他玩家加入
                UserInfHandlerHelper.sendJoinRoomInfo(tableStatus, tableStatus.getRoomId(), money, tableUser, oldUsers);
            }


            //  通知玩家 移动桌子了
            TableAllUser $tableAllUser = TableAllUser.load(tableId);
            sendMoveTableInfos(tableStatus, $tableAllUser, addUserIds);
            // 判断游戏状态 如果游戏未开始 需要推送消息给manager 让manager开始

            if ($tableAllUser.getJoinTableUserMap().size() >= 6) { //TODO 达到6人开始,是否还有其他条件?
                if (!tableStatus.isStarted()) { // 游戏没开始, 发送个游戏结束消息给 game manager
                    LOG.info("tableStatus.getStarted:" + tableStatus.getStarted());
                    LOG.info("桌子开始,桌子ID:" + tableId + "人数:" + $tableAllUser.getJoinTableUserMap().size());
                    //发送消息
                    context.parent().tell(new LocalTableRoundEndEvent(tableId, event.gameId(), new ArrayList<>(), new HashMap<>()),
                            ActorRef.noSender());
                }
            } else {
                LOG.info("桌子:" + tableId + "增加人数后的人数:" + $tableAllUser.getJoinTableUserMap().size());
            }
        }
        finally {
            sender.tell("ok", ActorRef.noSender());
        }
    }

    private void sendMoveTableInfos(TableStatus tableStatus,TableAllUser tableAllUser, Collection<String> userIds) {
        JSONObject object = new JSONObject();
        object.put("inner_id", "");
        object.put("inner_cnt", "");
        object.put("table_id", tableStatus.getTableId());
        GameUtils.notifyUsers(object, GameEventType.CHANGE_TABLE, userIds,
                UUID.randomUUID().toString().replace("-", ""),
                tableStatus.getRoomId());
        for (String userId : userIds) {
             UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableStatus.getTableId(), userId, null, null, null);
        }
    }
}
