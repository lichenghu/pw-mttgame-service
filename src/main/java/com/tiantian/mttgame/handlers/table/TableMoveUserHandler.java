package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.LocalTableMoveUserEvent;
import com.tiantian.mttgame.data.redis.RedisUtil;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 *
 */
public class TableMoveUserHandler implements EventHandler<LocalTableMoveUserEvent> {
    private static Logger LOG = LoggerFactory.getLogger(TableMoveUserHandler.class);
    @Override
    public void handler(LocalTableMoveUserEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        try {
            String tableId = event.tableId();
            LOG.info("TableMoveUserHandler:" + JSON.toJSONString(event));
            TableAllUser tableAllUser = TableAllUser.load(tableId);
            Map<String, String> userInfos = new HashMap<>();
            for (String userId : event.getUserIds()) {
                TableUser tableUser = TableUser.load(userId, tableId);
                if (!tableUser.isNull()) {
                    userInfos.put(tableUser.getUserId(), tableUser.getSitNum());
                    tableUser.delSelf(tableId);
                    LOG.info("Table delete user: userId :" + userId + ", tableId : " + tableId);
                } else {
                    LOG.error("Table delete user not exists");
                    Set<String> keys = RedisUtil.getKeys(GameConstants.USER_SPINGO_TABLE_KEY + userId + ":*");
                    if (keys != null) {
                        for (String key : keys) {
                            String rmId = RedisUtil.getFromMap(key, "roomId");
                            String tbId = RedisUtil.getFromMap(key, "tableId");
                            LOG.info("Table id :" + tbId);
                            if (rmId != null && rmId.equalsIgnoreCase(event.gameId())) {
                                Map<String, String> map = RedisUtil.getMap(GameConstants.USER_SPINGO_TABLE_KEY + userId + ":" + tbId);
                                LOG.info("Delete User info:" + JSON.toJSONString(map));
                            }
                        }
                    }
                }
                tableAllUser.deleteUserByUserId(userId);
                tableAllUser.deleteOnline(userId);
            }
            //  判断是否还有人数
            TableAllUser $tableAllUser = TableAllUser.load(tableId);
            TableStatus tableStatus = TableStatus.load(tableId);
            if (tableStatus != null) {
                for (String userId : event.getUserIds()) {
                     userLeave(tableStatus.getInningId(), tableStatus.getIndexCount(), userId, tableStatus.getRoomId(),
                            $tableAllUser);
                }
                for (Map.Entry<String, String> entry : userInfos.entrySet()) {
                     standUpInfo(tableStatus.getInningId(), tableStatus.getIndexCount(), entry.getKey(), entry.getValue(),
                            tableStatus.getRoomId(), $tableAllUser);
                }
            }


            if ($tableAllUser == null || $tableAllUser.getJoinTableUserMap() == null ||
                    $tableAllUser.getJoinTableUserMap().isEmpty()) {
                if (tableStatus != null) {
                    sendCloseInfo(tableId, tableStatus.getInningId(), tableStatus.getIndexCount(), tableStatus.getRoomId());
                    tableStatus.delSelf();
                }
            }
        }
        finally {
            sender.tell("ok", ActorRef.noSender());
        }
    }

    //  通知观看的玩家房间关闭了
    private void sendCloseInfo(String tableId, String innerId, String innerCnt, String roomId) {
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("table_id", tableId);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.MTT_TABLE_CLOSE,
                    tableAllUser.getOnlineTableUserIds(), id, roomId);
    }

    private void userLeave(String innerId, String innerCnt, String userId, String roomId,  TableAllUser tableAllUser) {
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("user_id", userId);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object,  "user_leave",
                tableAllUser.getOnlineTableUserIds(), id, roomId);
    }

    private void standUpInfo(String innerId, String innerCnt,
                             String userId, String sitNum, String gameId, TableAllUser tableAllUser) {
        // 通知玩家离开
        JSONObject object1 = new JSONObject();
        object1.put("inner_id", innerId);
        object1.put("inner_cnt", innerCnt);
        object1.put("uid", userId);
        object1.put("sn", Integer.parseInt(sitNum));
        object1.put("reason", "");
        String id = UUID.randomUUID().toString().replace("-", "");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUsers(object1, GameEventType.STAND_UP, tableAllUser.getOnlineTableUserIds(), id, gameId);
    }
}
