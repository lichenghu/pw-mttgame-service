package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mttgame.akka.event.LocalTableRestEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.utils.GameUtils;

import java.util.UUID;

/**
 *
 */
public class TableRestHandler implements EventHandler<LocalTableRestEvent> {
    @Override
    public void handler(LocalTableRestEvent event, ActorRef self, UntypedActorContext context,
                        ActorRef sender) {
        String tableId = event.tableId();
        long restMills = event.getRestMills();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        tableStatus.setRestMills(restMills + "");
        tableStatus.setStartRestTimeMills(System.currentTimeMillis() + "");
        tableStatus.saveNotAddCnt();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        // 倒计时开始游戏
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("rest_secs", restMills / 1000);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.MTT_REST,
                 tableAllUser.getOnlineTableUserIds(), id, null);
    }
}
