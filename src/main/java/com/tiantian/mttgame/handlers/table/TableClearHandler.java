package com.tiantian.mttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.mttgame.akka.event.LocalClearTableEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;

import java.util.Map;

/**
 *
 */
public class TableClearHandler implements EventHandler<LocalClearTableEvent> {
    @Override
    public void handler(LocalClearTableEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String tableId = event.getTableId();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus != null) {
            tableStatus.delSelf();
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser != null) {
            Map<String, String> allUsers = tableAllUser.userSitDownUsers();
            if (allUsers.size() > 0) {
                for (String uId : allUsers.values()) {
                     TableUser tableUser = TableUser.load(uId, tableId);
                     if (!tableUser.isNull()) {
                          tableUser.delSelf(tableId);
                     }
                    UserChips userChips = UserChips.load(uId, event.gameId());
                    if (userChips != null) {
                        userChips.delSelf(event.gameId());
                    }
                }
            }
            tableAllUser.delSelf();
        }
    }
}
