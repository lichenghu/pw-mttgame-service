package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.mtt.akka.event.user.GameUserReturnSitEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;

/**
 *
 */
public class UserReturnSitHandler implements EventHandler<GameUserReturnSitEvent> {
    @Override
    public void handler(GameUserReturnSitEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        TableUser  tableUser = TableUser.load(event.userId(), event.getTableId());
        if (!tableUser.isNull()) {
            if (!tableUser.isHang()) {
                sender.tell(true, ActorRef.noSender());
                return;
            }
            tableUser.setStatus(GameConstants.USER_STATUS_WAIT);
            tableUser.setNotOperateCount("");
            tableUser.save();
            UserChips userChips = UserChips.load(event.userId(), event.gameId());
            if (userChips != null) {
                userChips.resetStatus(GameConstants.USER_STATUS_WAIT);
            }
        } else {
            sender.tell(false, ActorRef.noSender());
            return;
        }
        sender.tell(true, ActorRef.noSender());

    }
}
