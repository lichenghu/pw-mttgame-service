package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.mtt.akka.event.user.GameUserFastRaiseEvent;
import com.tiantian.mtt.akka.event.user.GameUserRaiseEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.Handlers;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserBetLog;

/**
 *
 */
public class UserFastRaiseHandler implements EventHandler<GameUserFastRaiseEvent> {
    @Override
    public void handler(GameUserFastRaiseEvent userFastRaiseEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {

        String userId = userFastRaiseEvent.getUserId();
        String type = userFastRaiseEvent.getType();
        String pwd = userFastRaiseEvent.getPwd();
        String tableId = userFastRaiseEvent.getTableId();
        TableUser tableUser = TableUser.load(userId, tableId);
        if (tableUser == null) {
            return;
        }
        TableStatus tableStatus = TableStatus.load(tableId);
        long bigBlindMoney = Long.parseLong(tableStatus.getBigBlindMoney());
        long totalPoolMoney = tableStatus.getTotalPoolMoney();
        long raiseChips = 0;
        long mod = 0;
        UserBetLog userBetLog = tableStatus.getUserBetLog(tableUser.getSitNum());
        long roundChips = 0;
        if (userBetLog != null) {
            roundChips = userBetLog.getRoundChips();
        }
        GameUserRaiseEvent userRaiseEvent = null;
        switch (type) {
            case "triple_big_blind" : // 3倍大盲
                raiseChips = 3 * bigBlindMoney - roundChips;
                break;
            case "fivefold_blind" : // 5倍大盲注
                raiseChips = 5 * bigBlindMoney - roundChips;
                break;
            case "half_pool" : // 1/2底池
                raiseChips = totalPoolMoney / 2;
                mod = raiseChips % bigBlindMoney;
                if (mod != 0 ) {
                    raiseChips = raiseChips - mod + bigBlindMoney;
                }
                break;
            case "two-thirds_pool" : // 2/3底池
                raiseChips =(totalPoolMoney * 2) / 3;
                mod = raiseChips % bigBlindMoney;
                if (mod != 0 ) {
                    raiseChips = raiseChips - mod + bigBlindMoney;
                }
                break;
            case "one_pool" : // 1底池
                raiseChips = totalPoolMoney;
                break;
            default:return;
        }
        userRaiseEvent = new GameUserRaiseEvent(userId, raiseChips, pwd, tableStatus.getRoomId());
        userRaiseEvent.setTbId(tableId);
        Handlers.INSTANCE.execute(userRaiseEvent, self, context, sender);
    }
}
