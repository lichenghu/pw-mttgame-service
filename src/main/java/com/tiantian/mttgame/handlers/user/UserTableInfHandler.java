package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.mtt.akka.event.user.GameUserTableInfoEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.mttgame.manager.model.*;

/**
 *
 */
public class UserTableInfHandler implements EventHandler<GameUserTableInfoEvent> {

    @Override
    public void handler(GameUserTableInfoEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.getTableId();
        TableStatus tableStatus = TableStatus.load(tableId);
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableId, userId, null, null, null);
    }
}
