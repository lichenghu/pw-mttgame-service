package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.user.GameUserAllinEvent;
import com.tiantian.mtt.akka.event.user.GameUserCheckEvent;
import com.tiantian.mtt.akka.event.user.GameUserFoldEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.Handlers;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;
import com.tiantian.mttgame.utils.GameUtils;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserAllinHandler implements EventHandler<GameUserAllinEvent> {
    @Override
    public void handler(GameUserAllinEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String pwd = event.getPwd();
        String tableId = event.getTableId();
        TableUser tableUser = TableUser.load(userId, tableId);
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            // 判断口令是否有效
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                // 校验玩家能否allin，然后allin
                UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
                String[] ops = tableStatus.getUserCanOps(userId, tableUser.getSitNum());
                String status = null;
                char op = ops[0].charAt(4);
                // 能不能allin
                if ('0' == op) {
                    // 判断是否能够看牌
                    char checkOp = ops[0].charAt(1);
                    if ('0' == checkOp) {
                        status = GameEventType.FOLD;
                        GameUserFoldEvent gameUserFoldEvent = new GameUserFoldEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserFoldEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserFoldEvent, self, context, sender);
                        return;
                    } else {
                        status = GameEventType.CHECK;
                        GameUserCheckEvent gameUserCheckEvent = new GameUserCheckEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserCheckEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserCheckEvent, self, context, sender);
                        return;
                    }
                } else {
                    status = GameEventType.ALLIN;
                }
                // 增加玩家的操作计数
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                long leftChips = userChips.getChips();
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                // 如果能够allin
                if (status.equalsIgnoreCase(GameEventType.ALLIN)) {
                    userChips.reduceAndFlushChips(leftChips);
                    //设置allin的数值
                    object.put("val", leftChips);
                    tableStatus.userBet(tableUser.getSitNum(), leftChips);
                    // 更新平均加注日志
                    tableStatus.updateUserBetRaiseLog(tableUser.getSitNum(), leftChips);
                    object.put("bottom_pool", tableStatus.getTotalPoolMoney());
                }
                object.put("left_chips", userChips.getChips());
                //设置玩家加注
                tableUser.setBetStatus(status);
                tableUser.save();

                String maxBetSitNum = tableStatus.getMaxBetSitNum();
                String maxBetSitNumChips = tableStatus.getMaxBetSitNumChips();
                //通知其他在玩家该玩家allin
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableStatus.getRoomId());

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableStatus.getRoomId());

                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(),
                        maxBetSitNum, tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean needShow = tableStatus.needShowAllCards();
                    if (needShow) { // 需要显示所有的手牌
                        List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                        JSONObject object2 = new JSONObject();
                        object2.put("inner_id", tableStatus.getInningId());
                        object2.put("inner_cnt", tableStatus.getIndexCount());
                        object2.put("all_cards", cardsList);
                        String id2 = UUID.randomUUID().toString().replace("-", "");
                        GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableStatus.getRoomId());
                    }
                    //需要保存数据到redis
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId(),
                            context, tableStatus.getRoomId());
                    return;
                }

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, maxBetSitNum, false, maxBetSitNumChips,
                        self, context, sender);
            }
        }
    }
}
