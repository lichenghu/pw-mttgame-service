package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.user.GameUserExitEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.model.*;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.UUID;

/**
 *
 */
public class UserExitHandler implements EventHandler<GameUserExitEvent> {
    static Logger LOG = LoggerFactory.getLogger(UserExitHandler.class);
    @Override
    public void handler(GameUserExitEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.getTableId();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            LOG.error("TableStatus is null");
            sender.tell(true, ActorRef.noSender());
            return;
        }
        // 移除在线玩家
        RoomUsers.removeOnlineUsers(tableStatus.getRoomId(), userId);
        TableUser tableUser = TableUser.load(userId, tableId);

        if (tableUser == null || tableUser.isNull() ) {
            LOG.info("tableUser is null");
            sender.tell(true, ActorRef.noSender());
            return;
        }
        // 通知其他人 该玩家离开
        TableAllUser tableAllUsers = TableAllUser.load(tableId);
        tableAllUsers.deleteOnline(userId);
        exitRoom(tableStatus, tableAllUsers, userId, tableUser.getSitNum(), tableUser.getNickName());
        if (StringUtils.isBlank(tableUser.getSitNum())) { // 已经站起了 不发送退出消息给其他人
            LOG.info("sitNum is null");
            sender.tell(true, ActorRef.noSender());
            return;
        }
        sender.tell(true, ActorRef.noSender());
    }

    private void exitRoom(TableStatus tableStatus, TableAllUser tableAllUsers, String userId, String sitNum,
                          String nickName) {
        if (StringUtils.isBlank(sitNum) || tableStatus.isStarted()) { //游戏开始后不发送退出消息
            return;
        }
        // 通知其他人 该玩家离开
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("uid", userId);
        object.put("sn", Integer.parseInt(sitNum));
        object.put("nick_name", nickName);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.EXIT_ROOM,
                tableAllUsers.getOnlineTableUserIds(), id, tableStatus.getRoomId());
    }
}
