package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.user.GameUserShowCardsEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import java.util.UUID;

/**
 *
 */
public class UserShowCardsHandler implements EventHandler<GameUserShowCardsEvent> {
    @Override
    public void handler(GameUserShowCardsEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.getTableId();
        TableUser tableUser = TableUser.load(userId, event.getTableId());
        if (tableUser == null) {
            return;
        }
        TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
        // 是在结算的时候直接亮牌
        if (tableStatus != null && GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            String handCards = tableStatus.getUserHandCards(tableUser.getSitNum());
            if (StringUtils.isBlank(handCards)) {
                return;
            }
            TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(tableUser.getSitNum()));
            object.put("hand_cards", handCards);
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.SHOW_CARDS, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());
        }
    }
}
