package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.mtt.akka.event.user.GameUserEnterEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.model.TableAllUser;
import com.tiantian.mttgame.manager.model.TableStatus;
import com.tiantian.mttgame.manager.model.TableUser;
import com.tiantian.mttgame.manager.model.UserChips;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class UserEnterHandler implements EventHandler<GameUserEnterEvent> {
    private static Logger LOG = LoggerFactory.getLogger(UserEnterHandler.class);
    @Override
    public void handler(GameUserEnterEvent event, ActorRef self, UntypedActorContext context,
                        ActorRef sender) {
        String tableId = event.getTableId();
        String userId = event.getUserId();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            LOG.error("TableStatus is null, tableId:" + tableId);
            sender.tell(false, ActorRef.noSender());
            return;
        }
        TableUser tableUser = TableUser.load(userId, tableId);
        if (tableUser == null || tableUser.isNull()) {
            LOG.error("TableUser is null, tableId:" + tableId + ",userId:" + userId);
            sender.tell(false, ActorRef.noSender());
            return;
        }
        if (GameConstants.USER_STATUS_HANG.equalsIgnoreCase(tableUser.getStatus())) {
            tableUser.setStatus(GameConstants.USER_STATUS_WAIT);
            tableUser.save();
            UserChips userChips = UserChips.load(userId, event.gameId());
            if (userChips != null) {
                userChips.resetStatus(GameConstants.USER_STATUS_WAIT);
            }
        }

        TableAllUser tableAllUser = TableAllUser.load(tableId);
        // 加入在线房间中
        tableAllUser.joinOnline(userId);

        UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableId, userId, tableUser.getNickName(),
                tableUser.getAvatarUrl(), tableUser.getGender());

        sender.tell(true, ActorRef.noSender());
    }
}
