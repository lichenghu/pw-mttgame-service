package com.tiantian.mttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.mtt.akka.event.user.GameUserCallEvent;
import com.tiantian.mtt.akka.event.user.GameUserCheckEvent;
import com.tiantian.mtt.akka.event.user.GameUserFoldEvent;
import com.tiantian.mttgame.handlers.EventHandler;
import com.tiantian.mttgame.handlers.Handlers;
import com.tiantian.mttgame.manager.constants.GameEventType;
import com.tiantian.mttgame.manager.constants.GameStatus;
import com.tiantian.mttgame.manager.model.*;
import com.tiantian.mttgame.utils.GameUtils;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserCallHandler implements EventHandler<GameUserCallEvent> {
    @Override
    public void handler(GameUserCallEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String pwd = event.getPwd();
        String tableId = event.getTableId();
        TableUser tableUser = TableUser.load(userId, event.getTableId());
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            // 判断状态
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                // 校验玩家是否能够跟注
                UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
                long maxBet = 0;
                try{
                    maxBet = Long.parseLong(tableStatus.getMaxBetSitNumChips());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // 计算已经下注额
                long userAlreadyBet = 0;
                // 设置call的值
                UserBetLog userBetLog = tableStatus.getUserBetLog(tableStatus.getCurrentBet());
                if (userBetLog != null) {
                    userAlreadyBet = userBetLog.getRoundChips();
                }
                long callChips = maxBet - userAlreadyBet;
                String[] ops = tableStatus.getUserCanOps(userId, tableStatus.getCurrentBet());
                String status = null;
                char op = ops[0].charAt(2);
                //System.out.println("call-4--");
                // 能不能跟注
                if ('0' == op) {
                    // 判断是否能够看牌
                    char checkOp = ops[0].charAt(1);
                    if ('0' == checkOp) {
                        status = GameEventType.FOLD;
                        GameUserFoldEvent gameUserFoldEvent = new GameUserFoldEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserFoldEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserFoldEvent, self, context, sender);
                        return;
                    } else {
                        status = GameEventType.CHECK;
                        GameUserCheckEvent gameUserCheckEvent = new GameUserCheckEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserCheckEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserCheckEvent, self, context, sender);
                        return;
                    }
                } else {
                    status = GameEventType.CALL;
                }
                // 增加玩家的操作计数
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                if (status.equalsIgnoreCase(GameEventType.CALL)) {
                    if (callChips <= 0) {
                        System.out.println("error: callChips is less or eq 0");
                        callChips = 0;
                    }
                    userChips.reduceAndFlushChips(callChips);
                    object.put("val", callChips);
                    // 玩家下注
                    tableStatus.userBet(tableUser.getSitNum(), callChips);
                }
                object.put("left_chips", userChips.getChips());
                object.put("bottom_pool", tableStatus.getTotalPoolMoney());
                tableUser.setBetStatus(status);
                tableUser.save();

                // 发送个桌子的玩家
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableStatus.getRoomId());

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableStatus.getRoomId());

                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                        tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean needShow = tableStatus.needShowAllCards();
                    if (needShow) { // 需要显示所有的手牌
                        List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                        JSONObject object2 = new JSONObject();
                        object2.put("inner_id", tableStatus.getInningId());
                        object2.put("inner_cnt", tableStatus.getIndexCount());
                        object2.put("all_cards", cardsList);
                        String id2 = UUID.randomUUID().toString().replace("-", "");
                        GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableStatus.getRoomId());
                    }
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId(),
                            context, tableStatus.getRoomId());
                    return;
                }

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, null, false, null,
                        self, context, sender);
            }
        }
    }
}
