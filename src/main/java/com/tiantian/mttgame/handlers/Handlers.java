package com.tiantian.mttgame.handlers;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;

import com.tiantian.mtt.akka.event.Event;
import com.tiantian.mttgame.handlers.table.*;
import com.tiantian.mttgame.handlers.task.*;
import com.tiantian.mttgame.handlers.user.*;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameStatus;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class Handlers {

    public static Handlers INSTANCE = new Handlers();
    private static  Map<String, EventHandler> eventHandlers;

    private Handlers() {
        eventHandlers = new ConcurrentHashMap<>();
        init();
    }

    private void registerHandler(String name, EventHandler handler) {
        eventHandlers.put(name, handler);
    }


    private void init() {

        registerHandler(GameStatus.BEGIN.name(), new TableBeginHandler());
        registerHandler(GameStatus.D_AND_B.name(), new TableDAndBHandler());
        registerHandler(GameStatus.PRE_FLOP.name(), new TablePreFlopHandler());
        registerHandler(GameStatus.FLOP.name(), new TableFlopHandler());
        registerHandler(GameStatus.TURN.name(), new TableTurnHandler());
        registerHandler(GameStatus.RIVER.name(), new TableRiverHandler());
        registerHandler(GameStatus.FINISH.name(), new TableFinishHandler());

        registerHandler(GameConstants.CHECK_CHIPS, new TableCheckChipsHandler());
        registerHandler(GameConstants.TEST_STATUS, new TableTestStatusHandler());

        registerHandler(GameConstants.TEST_BET, new TableTestBetHandler());
        registerHandler(GameConstants.USER_REBUY_TASK, new TableTestRebuyHandler());


        registerHandler(GameConstants.USER_RAISE, new UserRaiseHandler());
        registerHandler(GameConstants.USER_FAST_RAISE, new UserFastRaiseHandler());
        registerHandler(GameConstants.USER_ALLIN, new UserAllinHandler());
        registerHandler(GameConstants.USER_CALL, new UserCallHandler());
        registerHandler(GameConstants.USER_CHECK, new UserCheckHandler());
        registerHandler(GameConstants.USER_FOLD, new UserFoldHandler());

        registerHandler(GameConstants.SHOW_CARDS, new UserShowCardsHandler());
        registerHandler(GameConstants.TABLE_INFO, new UserTableInfHandler());

        registerHandler(GameConstants.USER_EXIT, new UserExitHandler());
        registerHandler(GameConstants.USER_ENTER, new UserEnterHandler());

        registerHandler(GameConstants.TABLE_START, new TableStartHandler());
        registerHandler(GameConstants.USER_JOIN, new TableUserJoinHandler());

        registerHandler(GameConstants.ADD_USER, new TableAddUserHandler());
        registerHandler(GameConstants.MOVE_USER, new TableMoveUserHandler());

        registerHandler(GameConstants.UPGRADE_BLIND, new TableUpgradeBlindHandler());

        registerHandler(GameConstants.UPDATE_RANKING, new TableUpdateRankingHandler());

        registerHandler(GameConstants.USER_REBUY, new UserRebuyHandler());

        registerHandler(GameConstants.USER_RETURN_SIT, new UserReturnSitHandler());

        registerHandler(GameConstants.TABLE_CLEAR, new TableClearHandler());

        registerHandler(GameConstants.TABLE_REST, new TableRestHandler());
    }

    public void execute(Event tableEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String type = tableEvent.event();
        if (StringUtils.isBlank(type)) {
            throw new RuntimeException("event type can not be null");
        }
        EventHandler handler = eventHandlers.get(type);
        if (handler == null) {
            throw new RuntimeException("unregister event type :" + type);
        }
        try {
            handler.handler(tableEvent, self, context, sender);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
