package com.tiantian.mttgame.handlers;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;

/**
 *
 */
public interface EventHandler<T> {
     void handler(T event, ActorRef self, UntypedActorContext context, ActorRef sender);
}
