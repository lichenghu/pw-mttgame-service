package com.tiantian.mttgame.akka.event;
import com.tiantian.mtt.akka.event.Event;

/**
 *
 */
public interface LocalEvent extends Event {
    String gameId();
}
