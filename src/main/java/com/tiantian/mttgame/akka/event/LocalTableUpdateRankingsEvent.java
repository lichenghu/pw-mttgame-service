package com.tiantian.mttgame.akka.event;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class LocalTableUpdateRankingsEvent extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private long average;
    private List<Map<String, String>> userRankings;
    private int leftUsers;

    public LocalTableUpdateRankingsEvent(String tableId, String gameId, long average,
                                         List<Map<String, String>> userRankings, int leftUsers) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.average = average;
        this.userRankings = userRankings;
        this.leftUsers = leftUsers;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "updateRanking";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getAverage() {
        return average;
    }

    public void setAverage(long average) {
        this.average = average;
    }

    public List<Map<String, String>> getUserRankings() {
        return userRankings;
    }

    public void setUserRankings(List<Map<String, String>> userRankings) {
        this.userRankings = userRankings;
    }

    public int getLeftUsers() {
        return leftUsers;
    }

    public void setLeftUsers(int leftUsers) {
        this.leftUsers = leftUsers;
    }
}
