package com.tiantian.mttgame.akka.event;

import com.tiantian.mtt.akka.event.game.GameStartEvent;

/**
 *
 */
public class LocalUserJoinEvent extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private String joinUserId;
    private String nickName;
    private String avatarUrl;
    private String gender;
    private GameStartEvent.Game game;
    private boolean canStart;
    private int leftUser;
    private long averageChips;
    private int currentBlindLevel;
    private long nextUpBlindTimes;


    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "userJoin";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getJoinUserId() {
        return joinUserId;
    }

    public void setJoinUserId(String joinUserId) {
        this.joinUserId = joinUserId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public GameStartEvent.Game getGame() {
        return game;
    }

    public void setGame(GameStartEvent.Game game) {
        this.game = game;
    }

    public boolean isCanStart() {
        return canStart;
    }

    public void setCanStart(boolean canStart) {
        this.canStart = canStart;
    }

    public int getLeftUser() {
        return leftUser;
    }

    public void setLeftUser(int leftUser) {
        this.leftUser = leftUser;
    }

    public long getAverageChips() {
        return averageChips;
    }

    public void setAverageChips(long averageChips) {
        this.averageChips = averageChips;
    }

    public int getCurrentBlindLevel() {
        return currentBlindLevel;
    }

    public void setCurrentBlindLevel(int currentBlindLevel) {
        this.currentBlindLevel = currentBlindLevel;
    }

    public long getNextUpBlindTimes() {
        return nextUpBlindTimes;
    }

    public void setNextUpBlindTimes(long nextUpBlindTimes) {
        this.nextUpBlindTimes = nextUpBlindTimes;
    }
}
