package com.tiantian.mttgame.akka.event;

/**
 *
 */
public class LocalTableUpgradeBlindEvent  extends LocalTableEvent {

    private String tableId;
    private String gameId;
    private int upgradeSecs;
    private int nextLvl;
    private long nextSmallBlind;
    private long nextBigBlind;
    private long nextAnte;
    private long nextUpgradeTime;
    public LocalTableUpgradeBlindEvent(String tableId, String gameId, int upgradeSecs, int nextLvl,
                                       long nextSmallBlind, long nextBigBlind, long nextAnte, long nextUpgradeTime) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.upgradeSecs = upgradeSecs;
        this.nextLvl = nextLvl;
        this.nextSmallBlind = nextSmallBlind;
        this.nextBigBlind = nextBigBlind;
        this.nextAnte = nextAnte;
        this.nextUpgradeTime = nextUpgradeTime;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "upgradeBlind";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getUpgradeSecs() {
        return upgradeSecs;
    }

    public void setUpgradeSecs(int upgradeSecs) {
        this.upgradeSecs = upgradeSecs;
    }

    public int getNextLvl() {
        return nextLvl;
    }

    public void setNextLvl(int nextLvl) {
        this.nextLvl = nextLvl;
    }

    public long getNextSmallBlind() {
        return nextSmallBlind;
    }

    public void setNextSmallBlind(long nextSmallBlind) {
        this.nextSmallBlind = nextSmallBlind;
    }

    public long getNextBigBlind() {
        return nextBigBlind;
    }

    public void setNextBigBlind(long nextBigBlind) {
        this.nextBigBlind = nextBigBlind;
    }

    public long getNextAnte() {
        return nextAnte;
    }

    public void setNextAnte(long nextAnte) {
        this.nextAnte = nextAnte;
    }

    public long getNextUpgradeTime() {
        return nextUpgradeTime;
    }

    public void setNextUpgradeTime(long nextUpgradeTime) {
        this.nextUpgradeTime = nextUpgradeTime;
    }
}
