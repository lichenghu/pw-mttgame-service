package com.tiantian.mttgame.akka.event;

import java.util.List;

/**
 *
 */
public class LocalUpdateUserStatusEvent implements LocalEvent {
    private String gameId;
    private List<String> userIds;

    public LocalUpdateUserStatusEvent(String gameId, List<String> userIds) {
        this.gameId = gameId;
        this.userIds = userIds;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
