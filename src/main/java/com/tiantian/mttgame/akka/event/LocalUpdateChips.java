package com.tiantian.mttgame.akka.event;

/**
 *
 */
public class LocalUpdateChips implements LocalEvent {
    private String gameId;
    private String userId;
    private long leftChips;

    public LocalUpdateChips(String gameId, String userId, long leftChips) {
        this.gameId = gameId;
        this.userId = userId;
        this.leftChips = leftChips;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(long leftChips) {
        this.leftChips = leftChips;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

}
