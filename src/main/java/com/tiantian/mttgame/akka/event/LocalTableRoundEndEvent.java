package com.tiantian.mttgame.akka.event;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class LocalTableRoundEndEvent extends LocalTableEvent {
    private List<String> leaveUserIds;
    private String tableId;
    private String gameId;
    private Map<String, Long> userChipsMap;

    public LocalTableRoundEndEvent(String tableId, String gameId, List<String> leaveUserIds, Map<String, Long> userChipsMap) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.leaveUserIds = leaveUserIds;
        this.userChipsMap = userChipsMap;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    public List<String> getLeaveUserIds() {
        return leaveUserIds;
    }

    public void setLeaveUserIds(List<String> leaveUserIds) {
        this.leaveUserIds = leaveUserIds;
    }

    @Override
    public String event() {
        return null;
    }

    public Map<String, Long> getUserChipsMap() {
        return userChipsMap;
    }

    public void setUserChipsMap(Map<String, Long> userChipsMap) {
        this.userChipsMap = userChipsMap;
    }
}
