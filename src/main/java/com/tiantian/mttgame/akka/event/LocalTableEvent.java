package com.tiantian.mttgame.akka.event;

/**
 *
 */
public abstract class LocalTableEvent implements LocalEvent {
    public abstract String tableId();
}
