package com.tiantian.mttgame.akka.event;

/**
 *
 */
public class LocalUpdateRanking implements LocalEvent {
    private String gameId;
    private String userId;
    private int ranking;
    private String phyId;
    private int type;
    private long num;
    private boolean isGameOver;
    private long leftChips;
    private String gameName;
    private long gameStartTime;

    public LocalUpdateRanking(String gameId, String userId, int ranking, String phyId,
                              int type, long num, boolean isGameOver, long leftChips,
                              String gameName, long gameStartTime) {
        this.gameId = gameId;
        this.userId = userId;
        this.ranking = ranking;
        this.phyId = phyId;
        this.type = type;
        this.num = num;
        this.isGameOver = isGameOver;
        this.leftChips = leftChips;
        this.gameName = gameName;
        this.gameStartTime = gameStartTime;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public String getPhyId() {
        return phyId;
    }

    public void setPhyId(String phyId) {
        this.phyId = phyId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getNum() {
        return num;
    }

    public void setNum(long num) {
        this.num = num;
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public void setIsGameOver(boolean isGameOver) {
        this.isGameOver = isGameOver;
    }

    public long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(long leftChips) {
        this.leftChips = leftChips;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public long getGameStartTime() {
        return gameStartTime;
    }

    public void setGameStartTime(long gameStartTime) {
        this.gameStartTime = gameStartTime;
    }
}
