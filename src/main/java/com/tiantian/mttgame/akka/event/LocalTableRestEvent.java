package com.tiantian.mttgame.akka.event;

/**
 *
 */
public class LocalTableRestEvent extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private long restMills;

    public LocalTableRestEvent() {}

    public LocalTableRestEvent(String tableId, String gameId, long restMills) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.restMills = restMills;
    }
    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "tableRest";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public long getRestMills() {
        return restMills;
    }

    public void setRestMills(long restMills) {
        this.restMills = restMills;
    }
}
