package com.tiantian.mttgame.akka.event;

import com.tiantian.mttgame.manager.constants.GameConstants;

/**
 *
 */
public class LocalClearTableEvent implements LocalEvent {
    private String tableId;
    private String gameId;

    public LocalClearTableEvent(String tableId, String gameId) {
        this.tableId = tableId;
        this.gameId = gameId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return GameConstants.TABLE_CLEAR;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
