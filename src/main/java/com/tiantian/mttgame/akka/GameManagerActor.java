package com.tiantian.mttgame.akka;

import akka.actor.*;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tiantian.mtt.akka.event.Event;
import com.tiantian.mtt.akka.event.GameUserEvent;
import com.tiantian.mtt.akka.event.game.GameJoinStatusCheckEvent;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mtt.akka.event.user.GameUserDelaySignEvent;
import com.tiantian.mtt.akka.event.user.GameUserEnterEvent;
import com.tiantian.mtt.akka.event.user.GameUserExitEvent;
import com.tiantian.mttgame.akka.actor.GameActor;
import com.tiantian.mttgame.akka.actor.TableActor;
import com.tiantian.mttgame.akka.event.*;
import com.tiantian.mttgame.manager.constants.GameConstants;
import com.tiantian.mttgame.manager.constants.GameStatus;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class GameManagerActor extends UntypedActor {
    private static Logger LOG = LoggerFactory.getLogger(GameManagerActor.class);
    private Map<String, ActorRef> tableActorMap = new ConcurrentHashMap<>();
    private Map<String, TableInfo> tableInfos = new ConcurrentHashMap<>(); // TODO 持久化到redis中
    private Map<String, String> userTableIdMap = new ConcurrentHashMap<>(); //TODO 玩家的userId对应的桌子ID
    private static final int TABLE_MIN_USERS = 6;
    private static final int TABLE_MAX_USERS = 9;
    private List<String> checkTables = new ArrayList<>(); // 需要检查人的桌子ID
    private String gameId;
    private GameStartEvent.Game game;
    private String mttStatus = "wait"; //mtt游戏状态 start end
    private Set<String> gameEndUserIds = new HashSet<>();
    private Map<String, Long> userChips = new HashMap<>(); // 玩家userId和筹码
    private ActorRef roomDateActor;
    private int currentBlindLvl; // 当前盲注级别
    private long nextUpgradeTime; // 下次涨忙时间戳

    private final long finalTableRestMills = 300000;  // 最终桌休息时长
    private boolean finalTableHasRest = false;

    public GameManagerActor(String gameId) {
        this.gameId = gameId;
    }

    public void preStart() {
        roomDateActor = getContext().actorOf(Props.create(GameActor.class, gameId)
                .withDispatcher("dbDispatcher"), "RoomDate_" + gameId);
    }

    public void postStop() {
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) {
            selectEvent((Event)message);
        }
        else if (message instanceof GameTask) { //游戏里定时任务,向上级提交
            context().parent().tell(message, ActorRef.noSender());
        }
        else {
            unhandled(message);
        }
    }
    //TODO 游戏结束处理需要判断游戏当前是否是延迟报名,如果是则所有奖项都是第一个人的
    private void selectEvent(Event message) {
        if (message instanceof GameUserEvent) { // 玩家操作消息
            handleGameUserEvent((GameUserEvent) message);
        }
        else if (message instanceof GameStartEvent) { // 游戏开始消息
                game = ((GameStartEvent) message).getGame();
                beginGame(((GameStartEvent) message).getUserIds(), ((GameStartEvent) message).getUserInfos());
                sender().tell(true, ActorRef.noSender());
        } else if (message instanceof GameJoinStatusCheckEvent) {
                String userId = ((GameJoinStatusCheckEvent) message).getUserId();
                if(userTableIdMap.containsKey(userId)) {
                    sender().tell(true, ActorRef.noSender());
                } else {
                    sender().tell(false, ActorRef.noSender());
                }
        }
        else if (message instanceof TableTaskEvent) { // 定时任务
            tellChildTable(((TableTaskEvent) message).tableId(), message);
        }
        else if (message instanceof LocalEvent) {
            if (message instanceof LocalTableRoundEndEvent) { // 每局结束消息
                handleRoundEnd((LocalTableRoundEndEvent)message);
            }
            else if (message instanceof LocalGameEvent) { // 游戏定时事件
                handGameEvent(((LocalGameEvent) message).gameId(), ((LocalGameEvent) message).getEvent());
            } else if (message instanceof LocalUserRebuyEvent) {
                handleUserReBuy(((LocalUserRebuyEvent) message).getTableId(), ((LocalUserRebuyEvent) message).getUserId(),
                        ((LocalUserRebuyEvent) message).getBuyChips());
            }
        }

    }

    private void beginGame(List<String> allUsers, List<GameStartEvent.UserInfo> userInfos) { // 游戏开始  进行分桌
        currentBlindLvl = 1;
        if (game.getRules() != null && game.getRules().size() > 0) {
            nextUpgradeTime = System.currentTimeMillis() +  game.getRules().get(0).getUpgradeSecs() * 1000 + GameConstants.INIT_TO_BEGIN_DELAYER_TIME;
        }
         List<String> tableUsers = new ArrayList<>();
         List<GameStartEvent.UserInfo> mttTableUsers = new ArrayList<>();
         Map<String, GameStartEvent.UserInfo> userIdMttUsers = new HashMap<>();
         for (GameStartEvent.UserInfo userInfo : userInfos) {
             String uId = userInfo.getUserId();
             userIdMttUsers.put(uId, userInfo);
         }
         Collections.shuffle(allUsers); // 随机打乱
         int totalUsers = allUsers.size();
         long averageChips = (long)game.getStartBuyIn();
         for (int i = 0; i < allUsers.size(); i++) {
              tableUsers.add(allUsers.get(i));
              mttTableUsers.add(userIdMttUsers.get(allUsers.get(i)));
              if (i % 9 == 8 && i != 0) {
                  // 创建桌子
                  String tableId = createNewTable();
                  newTableJoinUsers(new ArrayList<>(tableUsers), tableId, new ArrayList<>(mttTableUsers), totalUsers, averageChips);
                  tableUsers.clear();
                  mttTableUsers.clear();
              }
         }
         if (tableUsers.size() > 0) {
             // 创建桌子
             String tableId = createNewTable();
             newTableJoinUsers(new ArrayList<>(tableUsers), tableId,  new ArrayList<>(mttTableUsers), totalUsers, averageChips);
             tableUsers.clear();
             mttTableUsers.clear();
         }
         LOG.info("beginGame: tables:" + JSON.toJSONString(tableInfos));
         LOG.info("beginGame: table_users:" + JSON.toJSONString(userTableIdMap));
         mttStatus = "start";

         tellBlindUpgrade(1);
         // 发送game定时检测事件, delay报名时候有可能只剩1个人了,delay后就直接获胜
         checkLastDelay();
         checkBlindUpgrade();
    }

    private String createNewTable() {
        String tableId = UUID.randomUUID().toString().replace("-", "");
        ActorRef tableActor = getContext().actorOf(Props.create(TableActor.class, tableId, gameId)
                .withDispatcher("dbDispatcher"), "MttTable_" + tableId);
        tableActorMap.put(tableId, tableActor);
        TableInfo tableInfo = new TableInfo();
        tableInfo.setStatus("wait");
        tableInfo.setTableId(tableId);
        tableInfo.setTableUsers(new ArrayList<>());
        tableInfos.put(tableId, tableInfo);
        return tableId;
    }

    private void newTableJoinUsers(List<String> userIds, String tableId, List<GameStartEvent.UserInfo> userInfos,
                                  int totalUsers, long averageChips) {
        ActorRef tableActor = tableActorMap.get(tableId);
        if (tableActor == null) {
            return;
        }
        boolean canStart = false;
        if (userIds.size() >= TABLE_MIN_USERS) {
            canStart = true;
        }
        TableInfo tableInfo = new TableInfo();
        tableInfo.setTableId(tableId);
        //tableInfo.setUserCnt(userInfos.size());
        tableInfo.setStatus(canStart ? "start" : "wait");
        List<TableUser> tableUsers = new ArrayList<>();
        for (GameStartEvent.UserInfo userInfo : userInfos) {
             TableUser tableUser = new TableUser();
             tableUser.setUserId(userInfo.getUserId());
             tableUser.setNickName(userInfo.getNickName());
             tableUser.setAvatarUrl(userInfo.getAvatarUrl());
             tableUsers.add(tableUser);
             userChips.put(userInfo.getUserId(), (long)game.getStartBuyIn());
        }
        tableInfo.setTableUsers(tableUsers);
        tableInfos.put(tableId, tableInfo);
        for (String userId : userIds) {
             userTableIdMap.put(userId, tableId);
        }
        roomDateActor.tell(new LocalUpdateUserStatusEvent(gameId, userIds), ActorRef.noSender());
        tableActor.forward(new LocalTableStartEvent(tableId, gameId, canStart, userIds, game, userInfos,
                totalUsers,  averageChips, nextUpgradeTime), getContext());
        if (!canStart) { // 入队有局结束时候检测
            roundEnd(tableId, new ArrayList<>());
        }
    }

    private void newUserJoin(String userId, String nickName, String gender, String avatarUrl) { // 游戏已经开始新玩家加入
        //获取最少人数的桌子,并且桌子状态未开始
        if (tableInfos.values().size() > 0) {
            List<TableInfo> tableList = new ArrayList<>(tableInfos.values());
            // 优先等待状态的
            Collections.sort(tableList, (o1, o2) -> {
                if ("wait".equals(o1.getStatus()) && !"wait".equals(o2.getStatus())) { // o1 是wait, o2不是
                    return -1;
                }
                if (!"wait".equals(o1.getStatus()) && "wait".equals(o2.getStatus())){ // o1 不是wait, o2 是
                    return 1;
                }
                int result = o1.getTableUsers().size() - o2.getTableUsers().size();
                if (result != 0) {
                    return result;
                }
                return o1.tableId.compareTo(o2.tableId);
            });
            TableInfo tableInfo = tableList.get(0);
            String joinTableId = tableInfo.tableId;
            boolean isNewTable = false;
            TableUser tableUser = new TableUser();
            tableUser.setUserId(userId);
            tableUser.setNickName(nickName);
            tableUser.setGender(gender);
            tableUser.setAvatarUrl(avatarUrl);
            if (tableInfo.getTableUsers().size() >= TABLE_MAX_USERS) {
                joinTableId = createNewTable();
                TableInfo newTableInfo = tableInfos.get(joinTableId);
                if (newTableInfo == null) {
                    LOG.error("newTableInfo is null, newTableId is " + joinTableId);
                } else {
                    newTableInfo.getTableUsers().add(tableUser);
                }
                isNewTable = true;
            } else {
                tableInfo.getTableUsers().add(tableUser);
            }
            userTableIdMap.put(userId, joinTableId);
            long chips = (long) game.getStartBuyIn();
            userChips.put(userId, chips); //
            if (isNewTable) { //新桌子
                GameStartEvent.UserInfo userInfo = new GameStartEvent.UserInfo();
                userInfo.setUserId(userId);
                userInfo.setNickName(nickName);
                userInfo.setAvatarUrl(avatarUrl);
                userInfo.setGender(gender);
                newTableJoinUsers(Lists.newArrayList(userId), joinTableId, Lists.newArrayList(userInfo),
                        userChips.size(), averageChips());
            }
            else { //添加到其他桌子里面
                LocalTableAddUserEvent.UserInfo userInfo = new LocalTableAddUserEvent.UserInfo();
                userInfo.setUserId(tableUser.getUserId());
                userInfo.setNickName(tableUser.getNickName());
                userInfo.setAvatarUrl(tableUser.getAvatarUrl());
                userInfo.setGender(tableUser.getGender());
                userInfo.setRanking(getUserRanking(tableUser.getUserId()) + "");
                LocalTableAddUserEvent addEvent = new LocalTableAddUserEvent(joinTableId, gameId,
                        Lists.newArrayList(userInfo));
                addEvent.setInitChips((long)game.getStartBuyIn());
                syncSend(joinTableId, 2, addEvent);
            }
        }
        roomDateActor.tell(new LocalUpdateUserStatusEvent(gameId, Lists.newArrayList(userId)),
                ActorRef.noSender());
        //更新排名信息
        refreshTableRanking();
    }

    private void handleGameUserEvent(GameUserEvent message) {
        String tableId = userTableIdMap.get(message.userId());
        if (message instanceof GameUserDelaySignEvent) { //玩家加入需要特殊处理
            if (StringUtils.isBlank(tableId)) {
                //玩家是延迟报名
                newUserJoin(((GameUserDelaySignEvent) message).getUserId(),
                        ((GameUserDelaySignEvent) message).getNickName(),
                        ((GameUserDelaySignEvent) message).getGender(),
                        ((GameUserDelaySignEvent) message).getAvatarUrl());
                return;
            }
        }
        if (StringUtils.isBlank(tableId)) {
            if (gameEndUserIds.contains(message.userId())) {
                if (message instanceof GameUserEnterEvent) { // 需要返回值
                    getSender().tell(false, ActorRef.noSender());
                } else if (message instanceof GameUserExitEvent) {
                    getSender().tell(true, ActorRef.noSender());
                }
            }
            return;
        }
        message.setTbId(tableId); // 设置桌子ID并发送桌子消息
        tellChildTable(tableId, message);
    }

    private void handleRoundEnd(LocalTableRoundEndEvent message) {
        LOG.info("begin RoundEnd: tables:" + JSON.toJSONString(tableInfos));
        LOG.info("begin RoundEnd: table_users:" + JSON.toJSONString(userTableIdMap));
        LOG.info("end RoundEnd: chips:" + JSON.toJSONString(userChips));
        //TODO 需要判断 未开始的游戏状态是wait桌子人数是否达到平均人数
        LOG.info("end table id :" + message.tableId() + "; leave ids :" + message.getLeaveUserIds());
        //TODO 需要判断分人之后其他桌子是否是暂停状态 如果是要通知开始
        roundEnd(message.tableId(), message.getLeaveUserIds());

        // 必须在roundEnd下面
        updateUserChips(message.getUserChipsMap());

        if (!isGameOver()) { //推送均码和玩家名次
            refreshTableRanking();
        }

        // TODO 判断是否需要同步开始
        if (userIsEnough(message.tableId())) { //判断人数是否小于2
            LOG.info("===开始游戏=====" + message.tableId());
            gameBegin(message.tableId());
        } else {
            LOG.info("=====游戏人数不足=====");
            if(isGameOver()) {
               gameEnd();
            }
        }
        LOG.info("end RoundEnd: tables:" + JSON.toJSONString(tableInfos));
        LOG.info("end RoundEnd: table_users:" + JSON.toJSONString(userTableIdMap));
        LOG.info("end RoundEnd: chips:" + JSON.toJSONString(userChips));
    }

    // 回合结束
    private void roundEnd(String tableId, List<String> leaveUserIds) {
        TableInfo tableInfo = tableInfos.get(tableId);
        if (tableInfo == null) {
            return;
        }
        LOG.info("回合前总平均人数" + averageTableUsers() +
                ",回合前桌子人数:" + tableInfo.getTableUsers().size() + ",桌子ID:" + tableId);
        //tableInfos.put(tableId, tableInfo);
        if (leaveUserIds != null) {
            for (String uId : leaveUserIds) {
                 gameEndUserIds.add(uId);
                 userTableIdMap.remove(uId);
                 List<TableUser> tableUsers = tableInfo.getTableUsers();
                 if (tableUsers != null) {
                     Iterator<TableUser> iterator = tableUsers.iterator();
                     while (iterator.hasNext()) {
                           TableUser tu = iterator.next();
                           if (tu.getUserId().equals(uId)) {
                               iterator.remove();
                               break;
                           }
                     }
                 }
            }
        }
        LOG.info(tableId + "回合后人数:" + tableInfo.getTableUsers().size() + ",回合后人:"
                + JSON.toJSONString(tableInfo.getTableUsers()));
        if (tableInfo.getTableUsers().size() <= TABLE_MIN_USERS) { // 游戏人数不足
            LOG.info(tableId + "回合后人数小于等于" + TABLE_MIN_USERS + ",添加到队列中");
            if (!checkTables.contains(tableId)) {
                checkTables.add(tableId);
            }
        }
        checkTableUsers(tableId);
    }
    //TODO 未处理特殊情况的桌数 比如2桌, 合桌子只剩一人
    private void checkTableUsers(String roundEndTableId) {
        if (userTableIdMap.size() <= TABLE_MAX_USERS * 2 && tableInfos.size() == 3) { // 最后3桌并且总人数小于等于 18人 直接合并2桌
            LOG.info("tables lt 3, users gt 18," + tableInfos.size() + "::" + userTableIdMap.size());
            checkTables.clear();
            Set<String> tIds = tableInfos.keySet();
            String firstId = null;
            String secondId = null;
            for (String tbId : tIds) {
                 if (!tbId.equalsIgnoreCase(roundEndTableId)) {
                     if (firstId == null) {
                         firstId = tbId;
                     } else {
                         secondId = tbId;
                     }
                 }
            }
            mergeOneToOtherTwoTables(roundEndTableId, firstId, secondId);
            return;
        }
        if (tableInfos.size() == 2) {
            if (userTableIdMap.size() <= TABLE_MAX_USERS) { // 正好只有9人
                LOG.info("tables eq 2, users gte 9," + tableInfos.size() + "::" + userTableIdMap.size());
                checkTables.clear();
                Set<String> tIds = tableInfos.keySet();
                String nextTableId = null;
                for (String tbId : tIds) {
                    if (!tbId.equalsIgnoreCase(roundEndTableId)) {
                            nextTableId = tbId;
                    }
                }
                mergeOneToAnother(roundEndTableId, nextTableId);
            }
            return;
        }
        if (checkTables.size() > 0) {
            int average = averageTableUsers();
            LOG.info("结束后桌子总平均人数" + average  + ",桌子ID:" + roundEndTableId);
            if (average <= TABLE_MIN_USERS) {
                LOG.info("结束后桌子总平均人数小于" + TABLE_MIN_USERS +  ",桌子ID:" + roundEndTableId);
                LOG.info("[开始合并桌子流程]" +  ",桌子ID:" + roundEndTableId);
                mergerTables2(roundEndTableId);
            } else {
                LOG.info("[开始移动桌子玩家流程]" + ",桌子ID:" + roundEndTableId); //TODO 有问题
                moveTableUsers2(roundEndTableId, average);
            }
        }
    }
    //TODO 人合并和移动后 没有游戏逻辑中没有处理 游戏开始情况
    private void mergerTables2(String roundEndTableId) {
        List<TableInfo> tableList = new ArrayList<>(tableInfos.values());
        // 优先等待状态的
        Collections.sort(tableList, (o1, o2) -> {
            if ("wait".equals(o1.getStatus()) && !"wait".equals(o2.getStatus())) { // o1 是wait, o2不是
                return -1;
            }
            if (!"wait".equals(o1.getStatus()) && "wait".equals(o2.getStatus())){ // o1 不是wait, o2 是
                return 1;
            }
            int result = o1.getTableUsers().size() - o2.getTableUsers().size();
            if (result != 0) {
                return result;
            }
            return o1.tableId.compareTo(o2.tableId);
        });
        LOG.info("[合并桌子流程]:桌子人数排名:" + JSON.toJSONString(tableList));
        TableInfo firstTable = tableList.remove(0);
        if (!firstTable.getTableId().equals(roundEndTableId)   //结束的桌子是最少的并且是已经开始了的
                && !"wait".equalsIgnoreCase(firstTable.getStatus())) { // 如果最少桌子是为开始的直接合并
            return;
        }
        int leftUsers = firstTable.getTableUsers().size();
        for (;;) {
            if(leftUsers <= 0) { //人数为空移除该桌子
               tableInfos.remove(firstTable.getTableId());
               return;
            }
            if (tableList.size() == 0) {
                return;
            }
            TableInfo secondTable = tableList.remove(0);
            //TODO 实现
//            int moveToUsers = average;
//            if (tableList.size() == 0) {
//                // 后面没有不足人数的桌子
//                 moveToUsers = TABLE_MAX_USERS; //直接移到最大人数
//            }
//            else { // 后面还有桌子人数不足的
//
//            }

            if (secondTable.getTableUsers().size() >= TABLE_MAX_USERS) { // 已经达到最大值,不能把第一个桌子合并进入第二个桌子
                return;
            }
            int needAdd = TABLE_MAX_USERS - secondTable.getTableUsers().size();
            if (needAdd > leftUsers) {
                checkTables.remove(roundEndTableId);
                tableList.add(0, secondTable);
                LOG.info("[合并桌子流程1.0]:第二张桌子ID:" + secondTable.getTableId() + ",合并后人数"
                        + secondTable.getTableUsers().size() + ",添加人数" + leftUsers + ",第一张桌子剩余人数:0");
                Object[] events = createLocalTableAddUserAndMoveUserEvent(firstTable.getTableId(),
                        secondTable.getTableId(), leftUsers);
                if (events != null) {
                    //移除该桌子
                    tableInfos.remove(firstTable.getTableId());

                    LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
                    LOG.info("LocalTableMoveUserEvent1:" + JSON.toJSONString(moveEvent));
                    LOG.info("firstTable.getTableId1:" + firstTable.getTableId());
                    // 通知桌子减少人
                    //   tellChildTable(firstTable.getTableId(), moveEvent);
                    syncSend(firstTable.getTableId(), 2, moveEvent);

                    LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
                    LOG.info("LocalTableAddUserEvent1:" + JSON.toJSONString(addEvent));
                    LOG.info("secondTable.getTableId1:" + secondTable.getTableId());
                    //通知桌子增加人
                  //  tellChildTable(secondTable.getTableId(), addEvent);
                    syncSend(secondTable.getTableId(), 2, addEvent);


                }
                return;
            } else {
                leftUsers -= needAdd;
                checkTables.remove(secondTable.tableId);
                LOG.info("[合并桌子流程1.1]:第二张桌子ID:" + secondTable.getTableId() + ",合并后人数"
                        + secondTable.getTableUsers().size() + ",添加人数" + needAdd + ",第一张桌子剩余人数:" + leftUsers);
                Object[] events = createLocalTableAddUserAndMoveUserEvent(firstTable.getTableId(),
                        secondTable.getTableId(), needAdd);
                if (events != null) {
                    LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
                    LOG.info("LocalTableMoveUserEvent:" + JSON.toJSONString(moveEvent));
                    LOG.info("firstTable.getTableId:" + firstTable.getTableId());
                    // 通知桌子减少人
                    syncSend(firstTable.getTableId(), 2, moveEvent);

                    LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
                    LOG.info("LocalTableAddUserEvent:" + JSON.toJSONString(addEvent));
                    LOG.info("secondTable.getTableId:" + secondTable.getTableId());
                    //通知桌子增加人
                    syncSend(secondTable.getTableId(), 2, addEvent);
                }
            }
        }
    }

    private void moveTableUsers2(String roundEndTableId, int average) {
        TableInfo roundEndTableInfo = tableInfos.get(roundEndTableId);
        if (roundEndTableInfo == null) {
            LOG.info("[移动桌子玩家流程] 结束桌子信息不存在");
            return;
        }
        if (roundEndTableInfo.getTableUsers().size() == 0) {
            tableInfos.remove(roundEndTableInfo.getTableId());
            return;
        }
        LOG.info("[移动桌子玩家流程] 结束桌子人数" + roundEndTableInfo.getTableUsers().size());
        LOG.info("[移动桌子玩家流程] 平均人数" + average);
        if (roundEndTableInfo.getTableUsers().size() <= average) {// 回合结束桌子小于最少人数
            LOG.info("[移动桌子玩家流程] 回合结束桌子小于最少人数不能把结束桌子人移到需要的桌子上,人数"
                    + roundEndTableInfo.getTableUsers().size());
            return;
        }
        TableInfo tableInfo;
        for (;;){
            if (checkTables.size() == 0) {
                LOG.info("[移动桌子玩家流程] checkTables size is 0");
                return;
            }
            String tableId = checkTables.remove(0); // 先移出第一个
            tableInfo = tableInfos.get(tableId);
            if (tableInfo != null) {
                break;
            }
        }
        int needUsersCnt = average - tableInfo.getTableUsers().size();
        if (needUsersCnt == 0) {
            LOG.info("[移动桌子玩家流程] needUsersCnt eq average; average is " + average);
            checkTables.add(0, tableInfo.getTableId());
            return;
        }
        int canRemoveCnt = roundEndTableInfo.getTableUsers().size() - average;
        if (canRemoveCnt <= 0) {
            // 再放回对列中
            LOG.info("[移动桌子玩家流程] re add to checkTables");
            checkTables.add(0, tableInfo.getTableId());
            return;
        }
        if (canRemoveCnt > needUsersCnt) {
            //TODO 发送消息,移动 needUSerCnt人数
            // roundEndTableInfo.userCnt = roundEndTableInfo.userCnt - needUsersCnt;
           // tableInfo.userCnt =  tableInfo.userCnt + needUsersCnt;
            LOG.info("[移动桌子玩家流程1.0] 移到的桌子ID:" + tableInfo.getTableId() + ",移到后桌子人数:"
                    + tableInfo.getTableUsers().size() + ",移动的人数:" + needUsersCnt);

            Object[] events = createLocalTableAddUserAndMoveUserEvent(roundEndTableId, tableInfo.getTableId(),
                    needUsersCnt);
            LOG.info("[移动桌子玩家流程1.2] 移动后移动桌子:" + JSON.toJSONString(roundEndTableInfo));
            if (events != null) {
                LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
                // 通知桌子减少人
                syncSend(roundEndTableId, 2, moveEvent);

                LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
                //通知桌子增加人
                syncSend(tableInfo.getTableId(), 2, addEvent);

            }
            //递归调用
            moveTableUsers2(roundEndTableId, average);
        } else {
            checkTables.add(0, tableInfo.getTableId());
            LOG.info("[移动桌子玩家流程1.1] 移到的桌子ID:" + tableInfo.getTableId() + ",移到后桌子人数:"
                    + tableInfo.getTableUsers().size() + ",移动的人数:" + needUsersCnt);
            Object[] events = createLocalTableAddUserAndMoveUserEvent(roundEndTableId, tableInfo.getTableId(),
                    canRemoveCnt);
            if (events != null) {
                LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
                // 通知桌子减少人
                syncSend(roundEndTableId, 2, moveEvent);

                LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
                //通知桌子增加人
                syncSend(tableInfo.getTableId(),2, addEvent);

            }
            if (roundEndTableInfo.getTableUsers().size() == 0) {
                tableInfos.remove(roundEndTableInfo.getTableId());
            }
        }
    }

    private void mergeOneToOtherTwoTables(String endTableId, String firstTableId, String secondTableId) {
        TableInfo firstTableInfo = tableInfos.get(firstTableId);
        TableInfo secondTableInfo = tableInfos.get(secondTableId);
        TableInfo endTableInfo = tableInfos.get(endTableId);

        int endNums = endTableInfo.getTableUsers().size();
        int firstNums = firstTableInfo.getTableUsers().size();
        int secondNums = secondTableInfo.getTableUsers().size();
        int average = (endNums + firstNums + secondNums) / 2; // 取整数
        LOG.info("endNums:" + endNums + ",firstNums:" + firstNums + ", secondNums:" + secondNums);
        int firstGet = 0;
        if (average > firstNums) {
            firstGet = average - firstNums;
        }

        int leftNums = endNums;
        if (firstGet > 0) {
            firstGet = Math.min(firstGet, leftNums);
            leftNums = leftNums - firstGet;
            Object[] events = createLocalTableAddUserAndMoveUserEvent(endTableId, firstTableId,
                    firstGet);
            if (events != null) {
                LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
                // 通知桌子减少人
                syncSend(endTableId, 2, moveEvent);

                LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
                //通知桌子增加人
                syncSend(firstTableId, 2, addEvent);

            }
        }

        if (leftNums > 0) {
            Object[] events = createLocalTableAddUserAndMoveUserEvent(endTableId, secondTableId,
                    leftNums);
            if (events != null) {
                LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
                // 通知桌子减少人
                syncSend(endTableId, 2, moveEvent);

                LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
                //通知桌子增加人
                syncSend(secondTableId, 2, addEvent);

            }
        }

        if (tableInfos.get(endTableId).getTableUsers().size() == 0) {
            LOG.info("end table users is 0");
            tableInfos.remove(endTableId);
        }
    }

    public void mergeOneToAnother(String endTableId, String anotherTableId) {
        TableInfo endTableInfo = tableInfos.get(endTableId);
        int leftNums = endTableInfo.getTableUsers().size();
        Object[] events = createLocalTableAddUserAndMoveUserEvent(endTableId, anotherTableId,
                leftNums);
        if (events != null) {

            LocalTableMoveUserEvent moveEvent = (LocalTableMoveUserEvent)events[1];
            // 通知桌子减少人
            syncSend(endTableId, 2, moveEvent);

            LocalTableAddUserEvent addEvent = (LocalTableAddUserEvent)events[0];
            //通知桌子增加人
            syncSend(anotherTableId, 2, addEvent);
        }
        if (tableInfos.get(endTableId).getTableUsers().size() == 0) {
            LOG.info("end table users is 0");
            tableInfos.remove(endTableId);
        }
    }

    private boolean userIsEnough(String tableId) {
        TableInfo roundEndTableInfo = tableInfos.get(tableId);
        if (roundEndTableInfo == null) {
            return false;
        }
        if (!(roundEndTableInfo.getTableUsers().size() >= 2)) {
            roundEndTableInfo.setStatus("wait");
        }
        return roundEndTableInfo.getTableUsers().size() >= 2;
    }


    private void handGameEvent(String gameId, String event) {
        if ("end".equalsIgnoreCase(mttStatus)) { // 已经结束
            return;
        }
        switch (event) {
            case  "check_delay" :
                if (userTableIdMap.size() == 1 && !isDelaying()) {
                    checkOnlyLast();
                    return;
                }
                break;
            case "blind_upgrade" :
                if (System.currentTimeMillis() >= nextUpgradeTime && nextUpgradeTime > 0) {
                    //升级
                    currentBlindLvl ++;
                    int upgradeSecs = 0;
                    long nextSB = 0;
                    long nextBB = 0;
                    long nextAnte = 0;
                    if (game.getRules() != null && game.getRules().size() >= currentBlindLvl) {
                        upgradeSecs = game.getRules().get(currentBlindLvl - 1).getUpgradeSecs();
                        nextSB = game.getRules().get(currentBlindLvl - 1).getSmallBlind();
                        nextBB =  game.getRules().get(currentBlindLvl - 1).getBigBlind();
                        nextAnte = game.getRules().get(currentBlindLvl - 1).getAnte();
                        nextUpgradeTime = System.currentTimeMillis() + upgradeSecs * 1000;
                    }
                    Set<String> tableIdSets = new HashSet<>(userTableIdMap.values());
                    for (String tbId : tableIdSets) {
                         tellChildTable(tbId, new LocalTableUpgradeBlindEvent(tbId, gameId, upgradeSecs,
                                 currentBlindLvl, nextSB, nextBB, nextAnte, nextUpgradeTime));
                    }
                    //更新盲注级别
                    tellBlindUpgrade(currentBlindLvl);
                }
                if (game.getRules().size() > currentBlindLvl) {
                    checkBlindUpgrade();
                }
                break;
            case "final_table_rest_over" :
                LOG.info("final table rest over");
                // 获取所有的桌子开始游戏
                if (tableInfos.size() > 0) {
                    tableInfos.keySet().forEach(this::gameBegin);
                }
                finalTableHasRest = true;
                break;
            default:
        }
        checkLastDelay();
    }

    public void handleUserReBuy(String tableId, String userId, long reBuyIn) {
         Long chips = userChips.get(userId);
         if (chips != null) {
             userChips.put(userId, reBuyIn + chips);
         } else {
             userChips.put(userId, reBuyIn);
         }
        roomDateActor.tell(new LocalUserRebuySuccessEvent(gameId, tableId, userId, reBuyIn), ActorRef.noSender());
    }

    private void checkLastDelay() {
        // 发送game定时检测事件, delay报名时候有可能只剩1个人了,delay后就直接获胜
        context().parent().tell(new LocalGameEvent(gameId, "check_delay", 5000), ActorRef.noSender());
    }

    private void checkBlindUpgrade() {
        // 发送game定时检测事件 监听涨盲注
        context().parent().tell(new LocalGameEvent(gameId, "blind_upgrade", 1000), ActorRef.noSender());
    }

    private void tellBlindUpgrade(int nextBlindLvl) {
        roomDateActor.tell(new LocalUpdateBlindEvent(gameId,  nextBlindLvl, nextUpgradeTime), ActorRef.noSender());
    }

    private int getUserRanking(String userId) {
        if (userChips.containsKey(userId)) {
            List<Map.Entry<String, Long>> list = new ArrayList<>(userChips.entrySet());
            Collections.sort(list, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
            int i = 0;
            long lastChips = -1;
            for (Map.Entry<String, Long> $entry : list) {
                 Long chips = $entry.getValue();
                 if (lastChips == -1 || lastChips != chips) {
                     i ++;
                 }
                 lastChips = chips;
                 String uId = $entry.getKey();
                 if (uId.equals(userId)) {
                     break;
                 }
            }
            return i;
        }
        return 0;
    }

    private void tellChildTable(String tableId, Object message) {
        if (StringUtils.isNotBlank(tableId)) {
            ActorRef tableActor = tableActorMap.get(tableId);
            if (tableActor == null) {
                LOG.error("tableActor is null, tableId:" + tableId + ",message:" + JSON.toJSONString(message));
                return;
            }
            tableActor.forward(message, getContext());
        }
    }

    private static SupervisorStrategy strategy =
            new OneForOneStrategy(
                    1, Duration.create("1 minute"),
                    t -> {
                        return SupervisorStrategy.resume();
                    });

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    private int averageTableUsers() {
        double totalTables = 0.0;
        double totalUsers = 0.0;
        for (TableInfo tableInfo : tableInfos.values()) {
             totalTables ++;
             totalUsers += tableInfo.getTableUsers().size();
        }
        LOG.info("计算总平均人数:总人数:" + totalUsers + ",总桌子数" + totalTables);
        return (int) Math.floor(totalUsers / totalTables);
    }


    private Object[] createLocalTableAddUserAndMoveUserEvent(String moveTableId, String toTableId, int size) {
        TableInfo tableInfo = tableInfos.get(moveTableId);
        if (tableInfo == null) {
            LOG.error("移动的桌子信息不存在,tableId" + moveTableId);
            return null;
        }
        List<TableUser> tableUsers = tableInfo.getTableUsers();
        if (tableUsers == null || tableUsers.size() == 0) {
            LOG.error("移动的桌子人没有人");
            return null;
        }
        if (tableUsers.size() < size) {
            LOG.error("移动的桌子人数不对, size:" + size + ",桌子剩余人数:" + tableUsers.size());
            return null;
        }
        TableInfo toTableInfo = tableInfos.get(toTableId);
        if (toTableInfo == null) {
            LOG.error("移动到的桌子信息不存在,tableId" + toTableId);
            return null;
        }
        List<TableUser> toTableUsers = toTableInfo.getTableUsers();
        if (toTableUsers == null) {
            toTableUsers = new ArrayList<>();
            toTableInfo.setTableUsers(toTableUsers);
        }
        if ((toTableUsers.size() + size) > TABLE_MAX_USERS) {
            LOG.error("移动到的桌子人数增加后超过最大人数" + TABLE_MAX_USERS + ",人数:" + toTableUsers.size() + "," + size);
            return null;
        }

        List<LocalTableAddUserEvent.UserInfo> userInfos = new ArrayList<>();
        List<String> moveUserIds = new ArrayList<>();
        while (size > 0) {
            size--;
            TableUser tableUser = tableUsers.remove(0); // 移除第一个人
            moveUserIds.add(tableUser.getUserId());
            // 增加桌子里面的人
            toTableUsers.add(tableUser);
            LocalTableAddUserEvent.UserInfo userInfo = new LocalTableAddUserEvent.UserInfo();
            userInfo.setUserId(tableUser.getUserId());
            userInfo.setNickName(tableUser.getNickName());
            userInfo.setAvatarUrl(tableUser.getAvatarUrl());
            userInfo.setGender(tableUser.getGender());
            userInfo.setRanking(getUserRanking(tableUser.getUserId()) + "");
            userInfos.add(userInfo);
            // 移动玩家所在的桌子
            userTableIdMap.put(tableUser.getUserId(), toTableId);
        }

        return new Object[]{new LocalTableAddUserEvent(toTableId, gameId, userInfos),
                            new LocalTableMoveUserEvent(moveTableId, gameId, moveUserIds)};
    }

    public void updateUserChips(Map<String, Long> userChipsMap) {
        if (userChipsMap != null) {
            List<Map.Entry<String, Long>> chipsEqZeros = Lists.newArrayList();
            List<Map.Entry<String, Long>> chipsNeqZeros = Lists.newArrayList();
            for (Map.Entry<String, Long> entry : userChipsMap.entrySet()) {
                if (entry.getValue() == 0) {
                    chipsEqZeros.add(entry);
                } else {
                    chipsNeqZeros.add(entry);
                }
            }
            List<Map.Entry<String, Long>> sorChipsList = new ArrayList<>(userChips.entrySet());
            Collections.sort(sorChipsList, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
            for (Map.Entry<String, Long> entry : chipsEqZeros) {
                String userId = entry.getKey();
                Long userChip = userChips.get(userId);
                if (userChip == null) {
                    LOG.error("Not found user chips in userChips, userId is" + userId);
                    continue;
                }
                //  更新排名
                if (!isDelaying()) { // 在延时报名的时候没有排名不进行跟新
                    int i = 1;
                    for (Map.Entry<String, Long> $entry : sorChipsList) {
                         String uId = $entry.getKey();
                         if (uId.equals(userId)) {
                             break;
                         }
                         i++;
                    }
                    // i 是玩家排名
                    String phyId = null;
                    int type = - 1;
                    long num = 0;
                    if (game.getRewards().size() >= i) {
                        GameStartEvent.Reward reward = game.getRewards().get(i-1);
                        phyId = reward.getPhysicalId();
                        type = reward.getVirtualType();
                        num = reward.getRanking();
                    }
                    roomDateActor.tell(new LocalUpdateRanking(gameId, userId, i, phyId, type, num, isGameOver(), 0,
                                    game.getGameName(), game.getStartMill()),
                            ActorRef.noSender());
                } else { // 延迟中 只更新状态 没有排名
                    roomDateActor.tell(new LocalUpdateRanking(gameId, userId, 0, null, 0, 0, isGameOver(), 0,
                                    game.getGameName(), game.getStartMill()),
                            ActorRef.noSender());
                }
            }

            for (Map.Entry<String, Long> entry : chipsEqZeros) {
                 String userId = entry.getKey();
                 userChips.remove(userId);
            }

            for (Map.Entry<String, Long> entry : chipsNeqZeros) {
                 String userId = entry.getKey();
                 Long chips = entry.getValue();
                 if (isGameOver()) {
                     String phyId = null;
                     int type = - 1;
                     long num = 0;
                     if (game.getRewards().size() > 0) {
                         GameStartEvent.Reward reward = game.getRewards().get(0);
                         phyId = reward.getPhysicalId();
                         type = reward.getVirtualType();
                         num = reward.getRanking();
                     }
                     roomDateActor.tell(new LocalUpdateRanking(gameId, userId, 1, phyId, type, num, true, chips,
                                     game.getGameName(), game.getStartMill()),
                             ActorRef.noSender());
                 }
                 else {
                     roomDateActor.tell(new LocalUpdateChips(gameId, userId, chips),
                             ActorRef.noSender());
                 }
                 userChips.put(userId, chips);
            }
        }
        if (isGameOver()) {
            checkOnlyLast();
        }
    }

    private void checkOnlyLast() {
        if (isGameOver()) {
            if(userChips.size() != 1) {
                LOG.error("game over error, userChips:" + JSON.toJSONString(userChips));
                return;
            }
            mttStatus = "end";
            List<Map.Entry<String, Long>> list = new ArrayList<>(userChips.entrySet());
            Map.Entry<String, Long> entry = list.get(0);
            String lastUserId = entry.getKey();
            Long lastChips = entry.getValue();
            String phyId = null;
            int type = - 1;
            long num = 0;
            if (game.getRewards().size() >= 1) {
                GameStartEvent.Reward reward = game.getRewards().get(0);
                phyId = reward.getPhysicalId();
                type = reward.getVirtualType();
                num = reward.getRanking();
            }
            roomDateActor.tell(new LocalUpdateRanking(gameId, lastUserId, 1, phyId, type, num, true, lastChips,
                            game.getGameName(), game.getStartMill()),
                    ActorRef.noSender());
        }
    }

    private void refreshTableRanking() {
        long averageChips = averageChips();
        Map<String, List<Map<String, String>>> rankingMap = new HashMap<>();
        for (Map.Entry<String, String> entry : userTableIdMap.entrySet()) {
            String userId = entry.getKey();
            String tableId = entry.getValue();
            List<Map<String, String>> rankings = rankingMap.get(tableId);
            if (rankings == null) {
                rankings = new ArrayList<>();
                rankingMap.put(tableId, rankings);
            }
            Map<String, String> userRankingInfo = new HashMap<>();
            userRankingInfo.put("userId", userId);
            userRankingInfo.put("ranking",  getUserRanking(userId) + "");
            rankings.add(userRankingInfo);
        }
        // 通知table更新
        for (Map.Entry<String, List<Map<String, String>>> entry : rankingMap.entrySet()) {
             tellChildTable(entry.getKey(), new LocalTableUpdateRankingsEvent(entry.getKey(), gameId,
                    averageChips, entry.getValue(), userChips.size()));
        }
    }

    private void gameBegin(String tableId) {
        if (checkNeedFinalTableRest()) {
            LOG.info("final table rest");
            return;
        }
        TableInfo tableInfo = tableInfos.get(tableId);
        if (tableInfo != null) {
            if ("wait".equalsIgnoreCase(tableInfo.getStatus())) {
                tableInfo.setStatus("start");
            }
        }
        JSONObject data = new JSONObject();
        data.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
        data.put("tableId", tableId);
        data.put("gameId", gameId);
        data.put("game_users", JSON.toJSONString(tableInfos.get(tableId).getTableUsers()));
        data.put("game_users_cnt", tableInfos.get(tableId).getTableUsers().size());
        String uIds = "";
        for (TableUser tableUser : tableInfos.get(tableId).getTableUsers()) {
            String id = tableUser.getUserId();
            uIds += (id + ",");
        }
        data.put("game_uids", uIds);
        TableTaskEvent tableTaskEvent = new TableTaskEvent(GameStatus.BEGIN.name(), data);
        tellChildTable(tableTaskEvent.tableId(), tableTaskEvent);
    }

    private void gameEnd() {
       //发送消息
       LOG.info("game end kill self");
       getSelf().tell(PoisonPill.getInstance(), ActorRef.noSender());
    }

    private boolean checkNeedFinalTableRest() {
        if(!finalTableHasRest && tableInfos.size() == 1) {

            context().parent().tell(new LocalGameEvent(gameId, "final_table_rest_over", finalTableRestMills),
                    ActorRef.noSender());

            Set<String> tbIds = tableInfos.keySet();
            String finalTableId = null;
            for (String tableId : tbIds) {
                finalTableId = tableId;
            }
            tellChildTable(finalTableId, new LocalTableRestEvent(finalTableId,
                    gameId, finalTableRestMills));
            return true;
        }
        return false;
    }

    private void syncSend(String tableId, int timeoutSecs, Object message) {
        ActorRef tableActor = tableActorMap.get(tableId);
        if (tableActor == null) {
            LOG.error("tableActor is null, tableId:" + tableId + ",message:" + JSON.toJSONString(message));
            return;
        }
        Timeout timeout = Timeout.apply(Math.max(1, timeoutSecs), TimeUnit.SECONDS);
        Future<Object> future = Patterns.ask(tableActor, message, timeout);
        try {
             Await.result(future, timeout.duration());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isDelaying() { //判断游戏是否在延时报名中
       return System.currentTimeMillis() < game.getStartMill() + game.getSignDelayMins() * 60000;
    }

    private boolean isGameOver() {
        return !isDelaying() && userTableIdMap.size() == 1;
    }


    private long averageChips() {
         Collection<Long> chips = userChips.values();
         if (chips.size() > 0) {
            long total = 0;
            for (Long chip : chips) {
                 total += chip;
            }
            return total / chips.size();
         }
         return 0;
    }

    private static class TableInfo {
        private String tableId;
        private String status; //wait start end
        private List<TableUser> tableUsers = new ArrayList<>();

        public String getTableId() {
            return tableId;
        }

        public void setTableId(String tableId) {
            this.tableId = tableId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<TableUser> getTableUsers() {
            return tableUsers;
        }

        public void setTableUsers(List<TableUser> tableUsers) {
            this.tableUsers = tableUsers;
        }
    }


    private static class TableUser implements Serializable {
        private String userId;
        private String nickName;
        private String avatarUrl;
        private String gender;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }

}
