package com.tiantian.mttgame.akka.actor;

import akka.actor.UntypedActor;
import com.alibaba.fastjson.JSON;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.mail.proxy_client.MailIface;
import com.tiantian.mttgame.akka.event.*;
import com.tiantian.mttgame.data.mongodb.MGDatabase;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 */
public class GameActor extends UntypedActor {
    private static Logger LOG = LoggerFactory.getLogger(GameActor.class);
    private static final String MTT_GAME = "mtt_game";
    private String gameId;

    public GameActor(String gameId) {
           this.gameId = gameId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        LOG.info("GameActor:" + JSON.toJSONString(message));
        if (message instanceof LocalUpdateRanking) {
            updateRanking((LocalUpdateRanking)message);
        } else if (message instanceof LocalUpdateUserStatusEvent) {
            long ret = updateUserStatus(((LocalUpdateUserStatusEvent) message).getUserIds());
            LOG.info("updateUserStatus counts:" + ret);
        }
        else if (message instanceof LocalUpdateChips) {
            boolean ret = updateLeftChips(((LocalUpdateChips) message).getUserId(),
                    ((LocalUpdateChips) message).getLeftChips());
            LOG.info("updateLeftChips ret:" + ret);
        } else if (message instanceof LocalUpdateBlindEvent) {
            updateBlind(((LocalUpdateBlindEvent) message).getCurrentBlindLvl(), ((LocalUpdateBlindEvent) message).getNextUpgradeTime());
        } else if (message instanceof LocalUserRebuySuccessEvent) {
            updateUserRebuy(((LocalUserRebuySuccessEvent) message).getBuyChips(),
                    ((LocalUserRebuySuccessEvent) message).getUserId());
        }
        else {
            unhandled(message);
        }
    }

    private void updateRanking(LocalUpdateRanking updateRanking) {
        LOG.info("Update Ranking:" + JSON.toJSONString(updateRanking));
        String userId = updateRanking.getUserId();
        String phyId = updateRanking.getPhyId();
        int ranking = updateRanking.getRanking();
        int type = updateRanking.getType();
        long num = updateRanking.getNum();
        boolean isOver = updateRanking.isGameOver();
        long leftChips = updateRanking.getLeftChips();
        LOG.info("Update Ranking1:");
        if (isOver) {
            updateMttOver();
            //TODO 发送邮件
        }
        LOG.info("Update Ranking2:");
        boolean updateRet = updateMttUserRanking(userId, ranking, leftChips);
        LOG.info("Update Ranking3:");
        System.out.println("UpdateRankgRet:" + updateRet);
        if (updateRet) {
            if (num > 0) {
                LOG.info("Update Ranking4:");
                try {
                    AccountIface.instance().iface().addUserMoney(userId, num);
                } catch (TException e) {
                    LOG.error("Add user money fail, userId:" + userId + ",money:" + num);
                    e.printStackTrace();
                }
            }
            if (StringUtils.isNotBlank(phyId)) {
                //TODO 增加玩家道具
            }
        } else {
            LOG.error("Update user ranking fail, gameId :" + gameId + ",userId:" + userId + ",ranking:" + ranking);
        }
        if (ranking > 0) {
            sendMail(userId, ranking, updateRanking.getGameName(), updateRanking.getGameStartTime());
        }
    }

    private long updateUserStatus(List<String> userIds) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject selectCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();
        for (String userId : userIds) {
            values.add(userId);
        }
        selectCondition.put("_id",  new ObjectId(gameId));
        selectCondition.put("mttTableUsers.userId", new BasicDBObject("$in", values));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("mttTableUsers.$.status", "gaming");
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateMany(selectCondition, updateSetValue);
        return result.getModifiedCount();
    }
    //TODO 有时候更新失败 为什么?
    private boolean updateLeftChips(String userId, long leftChips) {
        LOG.info("updateLeftChips:" + userId + ",leftChips:" + leftChips, ",gameId:" + gameId);
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        updateCondition.put("mttTableUsers.userId", userId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("mttTableUsers.$.leftChips", leftChips);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateMany(updateCondition, updateSetValue);
        LOG.info("updateLeftChips MatchedCount:" + result.getMatchedCount());
        LOG.info("updateLeftChips ModifiedCount:" + result.getModifiedCount());
        return (result.getModifiedCount() > 0);
    }

    private boolean updateMttUserRanking(String userId, int ranking, long leftChips) {

        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        updateCondition.put("mttTableUsers.userId", userId);
        BasicDBObject updatedValue = new BasicDBObject();
        if(ranking > 0) {
           updatedValue.put("mttTableUsers.$.ranking", ranking);
        }
        updatedValue.put("mttTableUsers.$.leftChips", leftChips);
        updatedValue.put("mttTableUsers.$.status", "end");
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateMany(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    private void updateMttOver() {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("status", -1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void updateBlind(int curretBlindLvl, long nextUpgradeTime) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("curretBlindLvl", curretBlindLvl);
        updatedValue.put("updateBlindTimes", nextUpgradeTime);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void updateUserRebuy(long addChips, String userId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        updateCondition.put("mttTableUsers.userId", userId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("mttTableUsers.$.leftChips", addChips);
        updatedValue.put("mttTableUsers.$.rebuyCnt", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$inc", updatedValue);
        UpdateResult updateResult = collection.updateOne(updateCondition, updateSetValue);
        LOG.info("updateUserRebuy:" + updateResult.getModifiedCount());
    }

    private void sendMail(String userId, int ranking, String gameName, long gameStartTime) {
        //  发送邮件
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            MailIface.instance().iface().saveNotice(userId,  "你在日期"+ sf.format(gameStartTime) + "的" + gameName + "比赛中获得了" + ranking + "名");
        } catch (TException e) {
            e.printStackTrace();
        }
    }

}
