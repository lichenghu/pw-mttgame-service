package com.tiantian.mttgame.akka.actor;


import akka.actor.UntypedActor;
import com.tiantian.mtt.akka.event.GameUserEvent;
import com.tiantian.mtt.akka.event.game.GameStartEvent;
import com.tiantian.mttgame.akka.event.LocalClearTableEvent;
import com.tiantian.mttgame.akka.event.LocalTableEvent;
import com.tiantian.mttgame.akka.event.LocalTableStartEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.mttgame.handlers.Handlers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TableActor extends UntypedActor {
    private static Logger LOG = LoggerFactory.getLogger(TableActor.class);
    private String tableId;
    private String gameId;
    private GameStartEvent.Game game;

    public TableActor(String tableId, String gameId) {
        this.tableId = tableId;
        this.gameId = gameId;
    }

    public void preStart() {
    }

    public void postStop() {
        LOG.info("game end clear tables");
        Handlers.INSTANCE.execute(new LocalClearTableEvent(tableId, gameId), getSelf(), getContext(), getSender());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof GameUserEvent) { //玩家操作事件
            Handlers.INSTANCE.execute((GameUserEvent) message, getSelf(), getContext(), getSender());
        }
        else if (message instanceof LocalTableEvent) {
            if (message instanceof LocalTableStartEvent) {
                game = ((LocalTableStartEvent) message).getGame();
                Handlers.INSTANCE.execute((LocalTableStartEvent) message, getSelf(), getContext(), getSender());
            }
            else if (message instanceof TableTaskEvent) { // 定时任务
                Handlers.INSTANCE.execute((TableTaskEvent) message, getSelf(), getContext(), getSender());
            }
            else {
                Handlers.INSTANCE.execute((LocalTableEvent) message, getSelf(), getContext(), getSender());
            }
        }
        else {
            unhandled(message);
        }
    }
}
