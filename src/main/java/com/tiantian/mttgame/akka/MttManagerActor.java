package com.tiantian.mttgame.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.alibaba.fastjson.JSON;
import com.tiantian.mtt.akka.event.Event;
import com.tiantian.mtt.akka.event.GameEvent;
import com.tiantian.mttgame.akka.event.GameTask;
import com.tiantian.mttgame.akka.event.LocalEvent;
import com.tiantian.mttgame.akka.event.LocalGameEvent;
import com.tiantian.mttgame.akka.event.TableTaskEvent;
import com.tiantian.timer.TaskWheel;
import org.apache.commons.lang.StringUtils;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class MttManagerActor extends UntypedActor {
    private Map<String, ActorRef> gameManagerActor = new ConcurrentHashMap<>();

    private TaskWheel<TableTaskEvent> taskTimeWheel;

    private TaskWheel<LocalGameEvent> gameWheel;

    public void preStart() {
        taskTimeWheel = new TaskWheel<>(elments -> {
            //System.out.println(JSON.toJSONString(elments));
            for (TableTaskEvent tableTaskEvent : elments) {
                 String gameId = tableTaskEvent.gameId();
                 tellChildTable(gameId, tableTaskEvent);
            }
        });
        taskTimeWheel.start();


        gameWheel = new TaskWheel<>(elments -> {
            //System.out.println(JSON.toJSONString(elments));
            for (LocalGameEvent event : elments) {
                 String gameId = event.gameId();
                 tellChildTable(gameId, event);
            }
        });
        gameWheel.start();
    }

    public void postStop() {
        taskTimeWheel.stop();
        gameWheel.stop();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) { //
            if (message instanceof GameEvent) {
                String gameId = ((GameEvent) message).gameId();
                ActorRef gameActor = gameManagerActor.get(gameId);
                if (gameActor == null) {
                    gameActor = getContext().actorOf(Props.create(GameManagerActor.class, gameId)
                            .withDispatcher("dbDispatcher"), "MttGameManager_" + gameId);
                    gameManagerActor.put(gameId, gameActor);
                }
                gameActor.forward(message, getContext());
            }
            else if (message instanceof LocalGameEvent) {
                gameWheel.offerTask((LocalGameEvent)message, ((LocalGameEvent) message).getDelay());
            }
        } else if (message instanceof GameTask) { //游戏里定时任务
                taskTimeWheel.offerTask(((GameTask) message).getTableTaskEvent(), ((GameTask) message).getDelayMill());
        }
//        else if (message instanceof GameOver) { // 关闭桌子
//            String tableId = ((GameOver) message).tableId();
//            if (StringUtils.isNotBlank(tableId)) {
//                ActorRef tableActor = gameManagerActor.remove(tableId);
//                if (tableActor != null) {
//                    tableActor.tell(PoisonPill.getInstance(), ActorRef.noSender());
//                }
//            }
//        }
        else {
            unhandled(message);
        }
    }

    private void tellChildTable(String gameId, Object message) {
        if (StringUtils.isNotBlank(gameId)) {
            ActorRef gameActor = gameManagerActor.get(gameId);
            //System.out.println(gameActor);
            if (gameActor == null) {
                return;
            }
            gameActor.forward(message, getContext());
        }
    }
}
